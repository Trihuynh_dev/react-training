import { Dispatch, SetStateAction, useState } from 'react';

// Hooks
import { useUserStore } from 'hooks';

// Types
import { IUser } from 'types/users';

// Enum
import { SIZE } from 'constants/enum';

// Components
import Avatar from 'components/Avatar/Avatar';
import TableCell from 'components/Table/TableCell/TableCell';
import TableRow from 'components/Table/TableRow/TableRow';
import TableRowEmpty from 'components/Table/TableRow/TableRowEmpty';
import Label from 'components/Label/Label';
import TableRowLoading from 'components/Table/TableRow/TableRowLoading';
import TableBody from 'components/Table/TableBody/TableBody';

// Actions
import { setUserSelected } from 'actions/user';

// Helpers
import { findItem } from 'helpers/find';

interface IProps {
  setSub: Dispatch<SetStateAction<boolean>>;
  data: IUser[];
}

const TableRowUser = ({ setSub, data }: IProps) => {
  const [indexSelected, setIndexSelected] = useState(0);
  const { userState, dispatchUser } = useUserStore();

  // Handle click on row selection
  const handleClickRow = (event: React.FormEvent<HTMLTableRowElement>) => {
    const idSelected = Number(event.currentTarget.id);
    setIndexSelected(idSelected);

    const userSelected = findItem<IUser>(idSelected, data);

    if (userSelected !== null) {
      dispatchUser(setUserSelected({ ...userState, userSelected: userSelected }));
    }

    setSub(true);
  };

  const renderRowTable = () => {
    if (data === null) {
      return <TableRowLoading />;
    }

    if (data.length === 0) {
      return <TableRowEmpty />;
    }

    return data.map((user) => (
      <TableRow key={user.id} id={user.id} index={indexSelected} handleClick={handleClickRow}>
        <TableCell>
          <Avatar name={user.name} size={SIZE.SMALL} circle avatar={user.avatar} />
        </TableCell>
        <TableCell>{user.name}</TableCell>
        <TableCell>
          <Label active={Boolean(user.status)} />
        </TableCell>
        <TableCell>{user.email}</TableCell>
      </TableRow>
    ));
  };

  return <TableBody>{renderRowTable()}</TableBody>;
};

export default TableRowUser;
