import { ChangeEvent, useCallback, useEffect, useMemo, useState } from 'react';

// Hooks
import { useDebounce, useStore, useUserStore } from 'hooks';

// Enums
import { SIZE } from 'constants/enum';

// Types
import { IMeta } from 'types/meta';
import { IRole } from 'types/role';
import { IRule } from 'types/rule';
import { IUser } from 'types/users';

// Components
import Table from 'components/Table/Table';
import TableHead from 'components/Table/TableHead/TableHead';
import Toolbar from 'components/Toolbar/Toolbar';
import GridColumn from 'layout/Grid/GridColumn/GridColumn';
import GridRow from 'layout/Grid/GridRow/GridRow';

// Sub
import TableRowUser from './TableBodyUser';
import UserManagerSub from './UserManagerSub';

// Actions
import { fetchUsersSuccess } from 'actions/user';
import { fetchFailed, fetching, fetchSuccess } from 'actions/loading';

// Services
import { fetchUsers } from 'services/user';
import { fetchMeta } from 'services/meta';
import { fetchRoles } from 'services/role';
import { fetchRules } from 'services/rule';

// Helpers
import { formatDataUsers } from 'helpers/formatData';
import { searchName } from 'helpers/search';

const UserManager = () => {
  const [isSubOpen, setIsSubOpen] = useState(false);
  const [search, setSearch] = useState('');
  const [data, setData] = useState({
    roles: [] as IRole[],
    rules: [] as IRule[],
    meta: {} as IMeta,
  });
  const { userState, dispatchUser } = useUserStore();
  const { loadingState, dispatchLoading } = useStore();

  const getData = async () => {
    dispatchLoading(fetching({ ...loadingState }));

    const fetchData = [fetchMeta(), fetchRoles(), fetchUsers(), fetchRules()];

    const data = await Promise.all(fetchData);

    const meta = data[0] as IMeta;
    const roles = data[1] as IRole[];
    const users = data[2] as IUser[];
    const rules = data[3] as IRule[];

    if (meta && roles && users && rules) {
      // Format data
      const usersFormated = formatDataUsers(users, meta, rules, roles);

      setData({
        roles: roles,
        rules: rules,
        meta: meta,
      });
      // Set actions to fetch success
      dispatchUser(fetchUsersSuccess({ ...userState, users: usersFormated }));

      dispatchLoading(fetchSuccess({ ...loadingState }));
    } else {
      dispatchLoading(fetchFailed({ ...loadingState }));
    }
  };

  useEffect(() => {
    getData();
  }, []);

  const setStateSub = useCallback(setIsSubOpen, [setIsSubOpen]);

  const searchValue = useDebounce(search, 500);
  const results = useMemo(
    () => searchName(userState.users!, searchValue, 'name'),
    [searchValue, userState.users],
  );

  // Handle change search
  const handleChangeSearch = (event: ChangeEvent<HTMLInputElement>) => {
    setSearch(event.target.value);
  };

  return (
    <GridRow>
      <GridColumn size={isSubOpen ? SIZE.MEDIUM : SIZE.XLARGE}>
        <Toolbar
          mode='search'
          name='Users'
          handleChangeSearch={handleChangeSearch}
          value={search}
          setValue={setSearch}
        />
        <Table>
          <TableHead listCell={['', 'Full Name', 'Status', 'Email']} />
          <TableRowUser setSub={setIsSubOpen} data={results} />
        </Table>
      </GridColumn>
      {isSubOpen && (
        <GridColumn size={SIZE.SMALL}>
          <UserManagerSub
            roles={data.roles}
            rules={data.rules}
            meta={data.meta}
            setSub={setStateSub}
          />
        </GridColumn>
      )}
    </GridRow>
  );
};

export default UserManager;
