import { ChangeEvent, memo, useCallback, useEffect, useRef, useState } from 'react';

// Hooks
import { useStore, useUserStore } from 'hooks';

// Enum
import { SIZE } from 'constants/enum';

// Types
import { IRole } from 'types/role';
import { IRule } from 'types/rule';
import { ITab } from 'types/tab';

// Components
import Avatar from 'components/Avatar/Avatar';
import IconButton from 'components/Button/IconButton/IconButton';
import SwitchButton from 'components/Button/SwitchButton/SwitchButton';
import InputFile from 'components/Input/InputFile/InputFile';
import InputText from 'components/Input/InputText/InputText';
import Label from 'components/Label/Label';
import Modal from 'components/Modal/Modal';
import TabPanel from 'components/Tabs/TabPanel/TabPanel';
import Tabs from 'components/Tabs/Tabs';
import Toolbar from 'components/Toolbar/Toolbar';

// Layouts
import EditCheckbox from 'layout/Form/FormEdit/EditCheckbox/EditCheckbox';
import EditInfomation from 'layout/Form/FormEdit/EditInfo/EditInfomation';
import EditRules from 'layout/Form/FormEdit/EditRules/EditRules';
import FormView from 'layout/Form/FormView/FormView';

// Actions
import { deleteUserSuccess, updateUserSuccess } from 'actions/user';

// Helpers
import { findItem, findListIdById } from 'helpers/find';
import { formatDataItem } from 'helpers/formatData';
import { removeUser, updateUser, uploadAvatar } from 'services/user';
import { IMeta } from 'types/meta';
import { fetchFailed, fetching, fetchSuccess } from 'actions/loading';

interface IProps {
  roles: IRole[];
  rules: IRule[];
  meta: IMeta;
  setSub: React.Dispatch<React.SetStateAction<boolean>>;
}

const UserManagerSub = ({ roles, rules, meta, setSub }: IProps) => {
  const [isHidden, setIsHidden] = useState(false);
  const [tabSelected, setTabSelected] = useState('1');
  const [stateModal, setStateModal] = useState(false);
  const [status, setStatus] = useState(false);
  const inputNameRef = useRef<HTMLInputElement>(null);
  const inputMailRef = useRef<HTMLInputElement>(null);

  const { userState, dispatchUser } = useUserStore();
  const { loadingState, dispatchLoading } = useStore();

  const { userSelected, users } = userState;

  const listTabs: Omit<ITab, 'index' | 'onClick'>[] = [
    {
      label: 'General',
      value: '1',
    },
    {
      label: 'Rules',
      value: '2',
    },
    {
      label: 'Roles',
      value: '3',
    },
  ];

  const allRuleAssigned = userSelected.allRules
    ? userSelected.allRules.map((rule) => findItem<IRule>(rule, rules))
    : null;

  useEffect(() => {
    if (isHidden === true && tabSelected === '1') {
      if (inputNameRef.current) {
        inputNameRef.current.focus();
      }
    }
  }, [isHidden, tabSelected]);

  useEffect(() => {
    setStatus(Boolean(userSelected.status));
  }, [userState]);

  // Select tab
  const handleSelectTab = (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    const tabSelected = event.currentTarget.value;
    setTabSelected(tabSelected);
  };

  // Checked
  const handleCheckedStatus = useCallback(setStatus, [setStatus]);

  // Handle change file
  const handleChangeFile = async (event: ChangeEvent<HTMLInputElement>) => {
    dispatchLoading(fetching({ ...loadingState }));

    const file = event.target.files![0];

    const formData = new FormData();
    formData.append('upload', file);
    formData.append('upload_fullpath', file.name);

    const res = await uploadAvatar(userSelected.id, formData);

    if (res) {
      const userUpdate = {
        ...userSelected,
        avatar: res.value,
      };

      const usersUpdate = userState.users!.map((user) => {
        if (user.id === userUpdate.id) {
          return userUpdate;
        }
        return user;
      });

      dispatchUser(
        updateUserSuccess({
          ...userState,
          userSelected: userUpdate,
          users: usersUpdate,
        }),
      );

      dispatchLoading(fetchSuccess({ ...loadingState }));
    } else {
      dispatchLoading(fetchFailed({ ...loadingState }));
    }
  };

  // Handle save user
  const handleSave = async () => {
    if (inputNameRef.current && inputMailRef.current) {
      if (inputNameRef.current.value === '') {
        alert('Please enter a full name');
      } else {
        dispatchLoading(fetching({ ...loadingState }));

        const userUpdate = {
          ...userSelected,
          name: inputNameRef.current.value,
          email: inputMailRef.current.value,
          status: status ? 1 : 0,
        };

        const res = await updateUser(userUpdate.id, userUpdate);

        if (res !== null) {
          const usersUpdate = userState.users!.map((user) => {
            if (user.id === userUpdate.id) {
              return userUpdate;
            }
            return user;
          });

          dispatchLoading(fetchSuccess({ ...loadingState }));

          dispatchUser(
            updateUserSuccess({
              ...userState,
              userSelected: userUpdate,
              users: usersUpdate,
            }),
          );
        } else {
          dispatchLoading(fetchFailed({ ...loadingState }));
        }
      }
    }
  };

  // Handle delete user
  const handleDelete = async () => {
    // Set loading state
    dispatchLoading(fetching({ ...loadingState }));

    const res = await removeUser(userSelected.id);

    if (res) {
      const usersUpdated = users!.filter((user) => user.id !== userSelected.id);

      dispatchLoading(fetchSuccess({ ...loadingState }));

      dispatchUser(deleteUserSuccess({ ...userState, users: usersUpdated }));

      setSub(false);
    } else {
      dispatchLoading(fetchFailed({ ...loadingState }));
    }
  };

  // Handle checked rule
  const handleCheckRule = async (id: string, checked: boolean) => {
    dispatchLoading(fetching({ ...loadingState }));

    let arrayRulesRoles = Object.entries(userSelected.RulesRoles!);

    let listRule = userSelected.rules;

    let allRulesAssigned = userSelected.allRules;

    if (listRule && allRulesAssigned) {
      if (checked) {
        listRule.push(Number(id));
        listRule = listRule.sort();

        if (!allRulesAssigned.includes(Number(id))) {
          allRulesAssigned.push(Number(id));
          allRulesAssigned = allRulesAssigned.sort();
        }

        const itemFind = arrayRulesRoles.find((item) => item[0] === id.toString());
        if (!itemFind) {
          arrayRulesRoles.push([id.toString(), []]);
        }
      } else {
        listRule = listRule.filter((item) => item !== Number(id));

        arrayRulesRoles = arrayRulesRoles.filter((item) => {
          if (item[0] === id && item[1].length === 0) {
            allRulesAssigned = allRulesAssigned!.filter((item) => item !== Number(id));
            return false;
          }

          return true;
        });
      }
    }

    const userUpdate = {
      ...userSelected,
      rules: listRule,
      RulesRoles: Object.fromEntries(arrayRulesRoles),
      allRules: allRulesAssigned,
    };

    const status = await updateUser(userUpdate.id, userUpdate);

    if (status !== null) {
      const usersUpdated = users!.map((user) => {
        if (user.id === userUpdate.id) {
          return userUpdate;
        }

        return user;
      });

      dispatchLoading(fetchSuccess({ ...loadingState }));

      dispatchUser(
        updateUserSuccess({
          ...userState,
          userSelected: userUpdate,
          users: usersUpdated,
        }),
      );
    } else {
      dispatchLoading(fetchFailed({ ...loadingState }));
    }
  };

  // Handle checked roles
  const handleCheckRole = async (id: string, checked: boolean) => {
    dispatchLoading(fetching({ ...loadingState }));

    let listRoles = userSelected.roles;
    let arrayRulesRoles = Object.entries(userSelected.RulesRoles!);
    let allRulesAssigned = userSelected.allRules;

    const listIdRules = findListIdById('rule', 'RoleRule', Number(id), meta, rules);

    if (listRoles && arrayRulesRoles) {
      if (checked) {
        // push id to users of role
        listRoles.push(Number(id));

        listIdRules.forEach((idRule) => {
          // Find item includes the rule
          const ruleRolesFind = arrayRulesRoles.find((item) => item[0] === idRule.toString());

          // arrayRulesRoles not found rule
          if (ruleRolesFind === undefined) {
            arrayRulesRoles.push([idRule.toString(), [Number(id)]]);
          }
          // arrayRulesRoles includes rule
          else {
            arrayRulesRoles = arrayRulesRoles.map((item) => {
              if (item[0] === idRule.toString()) {
                item[1].push(Number(id));
                item[1].sort();
              }

              return item;
            });
          }

          // Update allRulesAssigned
          if (allRulesAssigned!.includes(idRule) === false) {
            allRulesAssigned!.push(idRule);
            allRulesAssigned = allRulesAssigned!.sort();
          }
        });
      } else {
        // remove id from users of role
        listRoles = listRoles.filter((item) => item !== Number(id));

        arrayRulesRoles = arrayRulesRoles.filter((item) => {
          // If list id role has one element and it is role, return false
          if (
            item[1].length === 1 &&
            item[1].includes(Number(id)) &&
            !userSelected.rules?.includes(Number(item[0]))
          ) {
            allRulesAssigned = allRulesAssigned!.filter((number) => number !== Number(item[0]));
            return false;
          }

          // If list id role includes role, remove it and return true
          if (item[1].includes(Number(id))) {
            item[1] = item[1].filter((number) => number !== Number(id));
            return true;
          }

          return true;
        });
      }
    }

    // Update role
    const userUpdate = {
      ...userSelected,
      roles: listRoles,
      RulesRoles: Object.fromEntries(arrayRulesRoles),
      allRules: allRulesAssigned,
    };

    const res = await updateUser(userUpdate.id, userUpdate);

    if (res) {
      const usersUpdated = users!.map((user) => {
        if (user.id === userUpdate.id) {
          return userUpdate;
        }

        return user;
      });

      dispatchLoading(fetchSuccess({ ...loadingState }));

      dispatchUser(
        updateUserSuccess({
          ...userState,
          userSelected: userUpdate,
          users: usersUpdated,
        }),
      );
    } else {
      dispatchLoading(fetchFailed({ ...loadingState }));
    }
  };

  // Render form view
  const renderFormView = () => {
    return (
      <FormView
        hasAvatar
        avatar={userSelected.avatar}
        name={userSelected.name}
        email={{
          headerList: 'Email',
          icon: ['fas', 'envelope'],
          listItem: [{ id: '1', value: userSelected.email }],
        }}
        roles={{
          headerList: 'Roles assigned',
          icon: ['fas', 'user-shield'],
          listItem: userSelected.roles
            ? formatDataItem(
                userSelected.roles.map((role) => findItem<IRole>(role, roles)!),
                'id',
                'name',
              )
            : [],

          highlight: true,
          count: userSelected.roles ? userSelected.roles.length : 0,
        }}
        rules={{
          headerList: 'Rules assigned',
          icon: ['fas', 'list-check'],
          listItem: userSelected.allRules
            ? formatDataItem(
                userSelected.allRules.map((rule) => findItem<IRule>(rule, rules)!),
                'id',
                'long',
              )
            : [],
          highlight: true,
          count: userSelected.allRules!.length,
        }}
      />
    );
  };

  // Render form edit with tab
  const renderTabPanel = () => (
    <>
      <TabPanel index={tabSelected} value={'1'}>
        <EditInfomation
          nameBtnPrimary='Delete'
          handleBtnPrimary={() => setStateModal(true)}
          nameBtnSecondary='Save'
          handleBtnSecondary={handleSave}
        >
          <InputText
            ref={inputNameRef}
            size={SIZE.SMALL}
            label='Name'
            name='name'
            value={userSelected.name}
            pattern='([a-zA-Z]{2,30}\s*)+'
          />
          <InputText
            ref={inputMailRef}
            size={SIZE.SMALL}
            label='Email'
            name='email'
            value={userSelected.email}
            pattern='[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$'
          />

          <InputFile
            label='Avatar'
            htmlFor={'avatar'}
            name='Upload new photo'
            handleChange={handleChangeFile}
          />
          <div className='mg-left-l mg-top-s'>
            <Avatar size={SIZE.MEDIUM} avatar={userSelected.avatar} name={userSelected.name} />
          </div>
          <div className='input-box justify-start'>
            <p className='input-box__label'>Status:</p>
            <SwitchButton checked={status} handleClick={handleCheckedStatus} />
            <Label active={status} />
          </div>
        </EditInfomation>
      </TabPanel>
      <TabPanel index={tabSelected} value={'2'}>
        <EditRules
          hasCheckbox
          name={userSelected.name}
          listAllRules={rules}
          listDirectRules={userSelected.rules!.map((rule) => findItem<IRule>(rule, rules)!)}
          allRulesAssigned={allRuleAssigned}
          roles={roles}
          rulesRole={userSelected.RulesRoles}
          handleChange={handleCheckRule}
        />
      </TabPanel>
      <TabPanel index={tabSelected} value={'3'}>
        <EditCheckbox
          name={userSelected.name}
          type='Roles'
          listAll={roles}
          listAssign={userSelected.roles!.map((role) => findItem<IRole>(role, roles)!)}
          handleChange={handleCheckRole}
        />
      </TabPanel>
    </>
  );

  return (
    <>
      <div hidden={isHidden} className='sub'>
        <Toolbar
          name='User information'
          hasStatus
          statusActive={Boolean(userSelected.status)}
          mode='edit'
          onClick={() => setIsHidden(true)}
        />
        {renderFormView()}
      </div>
      <div hidden={!isHidden} className='sub'>
        <div className='d-flex border'>
          <IconButton icon={['fas', 'arrow-left']} onClick={() => setIsHidden(false)} />
          <Tabs list={listTabs} index={tabSelected} onClick={handleSelectTab} />
        </div>
        {renderTabPanel()}
      </div>
      <Modal
        title='Do you really want to delete this user?'
        stateModal={stateModal}
        btnNamePrimary={'Yes'}
        onClickBtnPrimary={handleDelete}
        btnNameSecondary={'No'}
        onClickBtnSecondary={() => setStateModal(false)}
        handleCloseModal={() => setStateModal(false)}
      />
    </>
  );
};

export default memo(UserManagerSub);
