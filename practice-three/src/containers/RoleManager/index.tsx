// React
import { ChangeEvent, useEffect, useState } from 'react';

// Enum
import { SIZE } from 'constants/enum';

// Types
import { IUser } from 'types/users';
import { IRule } from 'types/rule';
import { IMeta } from 'types/meta';
import { IRole } from 'types/role';

// Layouts
import GridRow from 'layout/Grid/GridRow/GridRow';
import GridColumn from 'layout/Grid/GridColumn/GridColumn';

// Components
import Toolbar from 'components/Toolbar/Toolbar';
import Table from 'components/Table/Table';
import TableHead from 'components/Table/TableHead/TableHead';

// Hooks
import { useDebounce, useRoleStore, useStore } from 'hooks';

// Action
import { fetchRolesSuccess } from 'actions/role';
import { fetchFailed, fetching, fetchSuccess } from 'actions/loading';

// Services
import { fetchMeta } from 'services/meta';
import { fetchRoles } from 'services/role';

// Helpers
import { formatDataRoles, formatDataUsers } from 'helpers/formatData';
import { searchName } from 'helpers/search';

// Services
import { fetchRules } from 'services/rule';
import { fetchUsers } from 'services/user';

// Sub
import RoleManagerSub from './RoleManagerSub';
import TableBodyRole from './TableBodyRole';

const RolesManager = () => {
  const [isSubOpen, setIsSubOpen] = useState(false);
  const [data, setData] = useState({ users: [] as IUser[], rules: [] as IRule[] });
  const [search, setSearch] = useState('');
  const { loadingState, dispatchLoading } = useStore();
  const { roleState, dispatchRole } = useRoleStore();

  const searchValue = useDebounce(search, 500);
  const results = searchName(roleState.roles!, searchValue, 'name');

  useEffect(() => {
    const getData = async () => {
      // Set loading
      dispatchLoading(fetching({ ...loadingState }));

      const fetchData = [fetchMeta(), fetchRoles(), fetchUsers(), fetchRules()];

      const data = await Promise.all(fetchData);

      const meta = data[0] as IMeta;
      const roles = data[1] as IRole[];
      const users = data[2] as IUser[];
      const rules = data[3] as IRule[];

      if (meta && roles && users && rules) {
        // Format data
        const rolesFormated = formatDataRoles(roles, meta, rules, users);
        const usersFormated = formatDataUsers(users, meta, rules, roles);

        setData({
          users: usersFormated,
          rules: rules,
        });

        // Set actions to fetch success
        dispatchLoading(fetchSuccess({ ...loadingState }));

        dispatchRole(
          fetchRolesSuccess({
            ...roleState,
            roles: rolesFormated,
          }),
        );
      } else {
        // Set actions to fetch failed
        dispatchLoading(fetchFailed({ ...loadingState }));
      }
    };

    getData();
  }, []);

  return (
    <GridRow>
      <GridColumn size={isSubOpen ? SIZE.MEDIUM : SIZE.XLARGE}>
        <Toolbar
          mode='search'
          name='Roles'
          handleChangeSearch={(event: ChangeEvent<HTMLInputElement>) =>
            setSearch(event.target.value)
          }
          value={search}
          setValue={setSearch}
        />
        <Table>
          <TableHead listCell={['', 'Name']} />
          <TableBodyRole setSub={setIsSubOpen} data={results} />
        </Table>
      </GridColumn>
      {isSubOpen && (
        <GridColumn size={SIZE.SMALL}>
          <RoleManagerSub users={data.users} rules={data.rules} setSub={setIsSubOpen} />
        </GridColumn>
      )}
    </GridRow>
  );
};

export default RolesManager;
