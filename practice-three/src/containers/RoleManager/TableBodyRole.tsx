// React
import { Dispatch, memo, SetStateAction, useState } from 'react';

// Custom hooks
import { useRoleStore } from 'hooks';

// Enum
import { SIZE } from 'constants/enum';

// Types
import { IRole } from 'types/role';

// Components
import Avatar from 'components/Avatar/Avatar';
import TableCell from 'components/Table/TableCell/TableCell';
import TableRow from 'components/Table/TableRow/TableRow';
import TableRowEmpty from 'components/Table/TableRow/TableRowEmpty';
import TableBody from 'components/Table/TableBody/TableBody';
import TableRowLoading from 'components/Table/TableRow/TableRowLoading';

// Actions
import { setRoleSelected } from 'actions/role';

// Helpers
import { findItem } from 'helpers/find';

interface IProps {
  data: IRole[];
  setSub: Dispatch<SetStateAction<boolean>>;
}

const TableBodyRole = ({ data, setSub }: IProps) => {
  const [indexSelected, setIndexSelected] = useState(0);
  const { roleState, dispatchRole } = useRoleStore();

  // Handle click on row selection
  const handleClickRow = (event: React.FormEvent<HTMLTableRowElement>) => {
    const idSelected = Number(event.currentTarget.id);
    setIndexSelected(idSelected);

    const roleSelected = findItem<IRole>(idSelected, data);

    if (roleSelected !== null) {
      dispatchRole(setRoleSelected({ ...roleState, roleSelected: roleSelected }));
    }

    setSub(true);
  };

  const renderRowTable = () => {
    if (data === null) {
      return <TableRowLoading />;
    }

    if (data.length === 0) {
      return <TableRowEmpty />;
    }

    if (data.length > 0) {
      return data.map((role) => (
        <TableRow key={role.id} id={role.id} index={indexSelected} handleClick={handleClickRow}>
          <TableCell>
            <Avatar name={role.name} size={SIZE.SMALL} circle color={role.color} />
          </TableCell>
          <TableCell>{role.name}</TableCell>
        </TableRow>
      ));
    }
  };

  return <TableBody>{renderRowTable()}</TableBody>;
};

export default memo(TableBodyRole);
