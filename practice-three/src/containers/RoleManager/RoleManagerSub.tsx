import { memo, useEffect, useRef, useState } from 'react';

// Types
import { IRule } from 'types/rule';
import { ITab } from 'types/tab';
import { IUser } from 'types/users';

// Hooks
import { useRoleStore, useStore } from 'hooks';

// Enums
import { SIZE } from 'constants/enum';

// Components
import IconButton from 'components/Button/IconButton/IconButton';
import ColorPicker from 'components/ColorPicker/ColorPicker';
import InputText from 'components/Input/InputText/InputText';
import TabPanel from 'components/Tabs/TabPanel/TabPanel';
import Tabs from 'components/Tabs/Tabs';
import Toolbar from 'components/Toolbar/Toolbar';
import EditInfomation from 'layout/Form/FormEdit/EditInfo/EditInfomation';
import FormView from 'layout/Form/FormView/FormView';

// Layouts
import EditCheckbox from 'layout/Form/FormEdit/EditCheckbox/EditCheckbox';
import EditRules from 'layout/Form/FormEdit/EditRules/EditRules';

// Helpers
import { findItem } from 'helpers/find';
import { formatDataItem } from 'helpers/formatData';

// Actions
import { deleteRoleSuccess, updateRoleSuccess } from 'actions/role';
import { fetchFailed, fetching, fetchSuccess } from 'actions/loading';

// Services
import { removeRole, updateRole } from 'services/role';
import { updateUser } from 'services/user';
import Modal from 'components/Modal/Modal';

interface IProps {
  users: IUser[];
  rules: IRule[];
  setSub: React.Dispatch<React.SetStateAction<boolean>>;
}

const RoleManagerSub = ({ users, rules, setSub }: IProps) => {
  const [isHidden, setIsHidden] = useState(false);
  const [tabSelected, setTabSelected] = useState('1');
  const [stateModal, setStateModal] = useState(false);
  const selectedColorRef = useRef<HTMLDivElement>(null);
  const inputNameRef = useRef<HTMLInputElement>(null);

  const { roleState, dispatchRole } = useRoleStore();
  const { loadingState, dispatchLoading } = useStore();

  const { roleSelected, roles } = roleState;

  const listTabs: Omit<ITab, 'index' | 'onClick'>[] = [
    {
      label: 'General',
      value: '1',
    },
    {
      label: 'Rules',
      value: '2',
    },
    {
      label: 'Members',
      value: '3',
    },
  ];

  const rulesAssign = formatDataItem(
    roleSelected.rules!.map((rule) => findItem<IRule>(rule, rules)!),
    'id',
    'long',
  );

  useEffect(() => {
    if (isHidden === true && tabSelected === '1') {
      if (inputNameRef.current) {
        inputNameRef.current.focus();
      }
    }
  }, [isHidden, tabSelected, roleSelected]);

  // Handle open edit
  const handleOpenEdit = () => {
    setIsHidden(true);
  };

  // Handle off edit
  const handleOffEdit = () => {
    setIsHidden(false);
    setTabSelected('1');
  };

  const handleSelectTab = (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    const tabSelected = event.currentTarget.value;
    setTabSelected(tabSelected);
  };

  // Handle checked
  const handleCheckUser = async (id: string, checked: boolean) => {
    dispatchLoading(fetching({ ...loadingState }));

    // Find user update roles
    const userUpdate = findItem<IUser>(Number(id), users);
    let rolesUser: number[] = [];

    if (userUpdate?.roles) {
      rolesUser = userUpdate.roles;
    }

    let listUser = roleSelected.users;

    if (listUser) {
      if (checked) {
        // push id to users of role
        listUser.push(Number(id));

        // push roleId into roles of user
        if (rolesUser) {
          rolesUser.push(roleSelected.id);
        }
      } else {
        // remove id from users of role
        listUser = listUser.filter((item) => item !== Number(id));

        // remove roleId from roles of user
        if (rolesUser) {
          rolesUser = rolesUser.filter((item) => item !== roleSelected.id);
        }
      }
    }

    if (userUpdate) {
      const payload = {
        ...userUpdate,
        roles: rolesUser,
      };

      const status = await updateUser(payload.id!, payload);

      if (status !== null) {
        // Update role
        const roleUpdate = {
          ...roleSelected,
          users: listUser,
        };

        // Push role update for list role
        const rolesUpdate = roles!.map((role) => {
          if (role.id === roleUpdate.id) {
            return roleUpdate;
          }

          return role;
        });

        dispatchLoading(fetchSuccess({ ...loadingState }));

        dispatchRole(
          updateRoleSuccess({
            roles: rolesUpdate,
            roleSelected: roleUpdate,
          }),
        );
      } else {
        dispatchLoading(fetchFailed({ ...loadingState }));
      }
    }
  };

  // Handle checked rule
  const handleCheckRule = async (id: string, checked: boolean) => {
    dispatchLoading(fetching({ ...loadingState }));

    let listRule = roleSelected.rules;

    if (listRule) {
      if (checked) {
        listRule.push(Number(id));
      } else {
        listRule = listRule.filter((item) => item !== Number(id));
      }
    }

    const roleUpdate = {
      ...roleSelected,
      rules: listRule,
    };

    const status = await updateRole(roleUpdate.id, roleUpdate);

    if (status !== null) {
      const rolesUpdate = roles!.map((role) => {
        if (role.id === roleUpdate.id) {
          return roleUpdate;
        }

        return role;
      });

      dispatchLoading(fetchSuccess({ ...loadingState }));

      dispatchRole(
        updateRoleSuccess({
          roleSelected: roleUpdate,
          roles: rolesUpdate,
        }),
      );
    } else {
      dispatchLoading(fetchFailed({ ...loadingState }));
    }
  };

  //Handle delete role
  const handleDelete = async () => {
    dispatchLoading(fetching({ ...loadingState }));

    const status = await removeRole(roleSelected.id);

    if (status !== null) {
      const rolesUpdate = roles!.filter((role) => role !== roleSelected);

      dispatchLoading(fetchSuccess({ ...loadingState }));

      dispatchRole(
        deleteRoleSuccess({
          ...roleState,
          roles: rolesUpdate,
        }),
      );

      setStateModal(false);
      setSub(false);
    } else {
      dispatchLoading(fetchFailed({ ...loadingState }));
    }
  };

  //Handle delete role
  const handleSave = async () => {
    if (inputNameRef.current && inputNameRef.current.value === '') {
      alert('Please enter a role name');
    } else {
      dispatchLoading(fetching({ ...loadingState }));

      const roleUpdate = {
        ...roleSelected,
        color: selectedColorRef.current
          ? selectedColorRef.current.style.backgroundColor
          : roleSelected.color,
        name: inputNameRef.current ? inputNameRef.current.value : roleSelected.name,
      };

      const status = await updateRole(roleUpdate.id, roleUpdate);

      if (status !== null) {
        const rolesUpdate = roles!.map((role) => {
          if (role.id === roleUpdate.id) {
            return roleUpdate;
          }
          return role;
        });

        dispatchLoading(fetchSuccess({ ...loadingState }));

        dispatchRole(
          updateRoleSuccess({
            roleSelected: roleUpdate,
            roles: rolesUpdate,
          }),
        );
      } else {
        dispatchLoading(fetchFailed({ ...loadingState }));
      }
    }
  };

  // Render edit
  const renderFormView = () => {
    return (
      <FormView
        hasAvatar
        name={roleSelected.name}
        color={roleSelected.color}
        users={{
          headerList: 'Members assigned',
          icon: ['fas', 'user-group'],
          listItem: formatDataItem(
            roleSelected.users!.map((user) => findItem<IUser>(user, users)!),
            'id',
            'name',
          ),
          highlight: true,
          count: roleSelected.users ? roleSelected.users.length : 0,
        }}
        rules={{
          headerList: 'Rules assigned',
          icon: ['fas', 'list-check'],
          listItem: rulesAssign,
          highlight: true,
          count: roleSelected.rules ? roleSelected.rules.length : 0,
        }}
      />
    );
  };

  const renderTabPanel = () => (
    <>
      <TabPanel index={tabSelected} value={'1'}>
        <EditInfomation
          nameBtnPrimary='Delete'
          handleBtnPrimary={() => setStateModal(true)}
          nameBtnSecondary='Save'
          handleBtnSecondary={handleSave}
        >
          <InputText
            ref={inputNameRef}
            size={SIZE.SMALL}
            label='Name'
            name='name'
            value={roleSelected.name}
          />
          <ColorPicker color={roleSelected.color} ref={selectedColorRef} />
        </EditInfomation>
      </TabPanel>
      <TabPanel index={tabSelected} value={'2'}>
        <EditRules
          name={roleSelected.name}
          listAllRules={rules}
          listDirectRules={roleSelected.rules!.map((rule) => findItem<IRule>(rule, rules)!)}
          handleChange={handleCheckRule}
        />
      </TabPanel>
      <TabPanel index={tabSelected} value={'3'}>
        <EditCheckbox
          name={roleSelected.name}
          type='Members'
          listAll={users}
          listAssign={roleSelected.users!.map((user) => findItem<IUser>(user, users)!)}
          handleChange={handleCheckUser}
        />
      </TabPanel>
    </>
  );

  return (
    <>
      <div hidden={isHidden} className='sub'>
        <Toolbar name='Role information' mode='edit' onClick={handleOpenEdit} />
        {renderFormView()}
      </div>
      <div hidden={!isHidden} className='sub'>
        <div className='d-flex border'>
          <IconButton icon={['fas', 'arrow-left']} onClick={handleOffEdit} />
          <Tabs list={listTabs} index={tabSelected} onClick={handleSelectTab} />
        </div>
        {renderTabPanel()}
      </div>
      <Modal
        title='Do you really want to delete this role?'
        stateModal={stateModal}
        btnNamePrimary={'Yes'}
        onClickBtnPrimary={handleDelete}
        btnNameSecondary={'No'}
        onClickBtnSecondary={() => setStateModal(false)}
        handleCloseModal={() => setStateModal(false)}
      />
    </>
  );
};

export default memo(RoleManagerSub);
