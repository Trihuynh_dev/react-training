import { Dispatch, memo, SetStateAction, useState } from 'react';

// Hooks
import { useRuleStore } from 'hooks';

// Types
import { IRule } from 'types/rule';

// Actions
import { setRuleSelected } from 'actions/rule';

// Components
import TableBody from 'components/Table/TableBody/TableBody';
import TableCell from 'components/Table/TableCell/TableCell';
import TableRow from 'components/Table/TableRow/TableRow';
import TableRowEmpty from 'components/Table/TableRow/TableRowEmpty';

// Helpers
import { findItem } from 'helpers/find';
import TableRowLoading from 'components/Table/TableRow/TableRowLoading';

interface IProps {
  data: IRule[];
  setSub: Dispatch<SetStateAction<boolean>>;
}

const TableBodyRule = ({ data, setSub }: IProps) => {
  const [indexSelected, setIndexSelected] = useState(0);
  const { ruleState, dispatchRule } = useRuleStore();

  const handleClickRow = (event: React.FormEvent<HTMLTableRowElement>) => {
    const idSelected = Number(event.currentTarget.id);
    setIndexSelected(idSelected);

    const ruleSelected = findItem<IRule>(idSelected, data);

    if (ruleSelected !== null) {
      dispatchRule(setRuleSelected({ ...ruleState, ruleSelected: ruleSelected }));
    }

    setSub(true);
  };

  const renderRowTable = () => {
    if (data === null) {
      return <TableRowLoading />;
    }

    if (data.length === 0) {
      return <TableRowEmpty />;
    }

    return data.map((rule) => (
      <TableRow key={rule.id} id={rule.id} index={indexSelected} handleClick={handleClickRow}>
        <TableCell>{rule.short}</TableCell>
        <TableCell>{rule.long}</TableCell>
      </TableRow>
    ));
  };

  return <TableBody>{renderRowTable()}</TableBody>;
};

export default memo(TableBodyRule);
