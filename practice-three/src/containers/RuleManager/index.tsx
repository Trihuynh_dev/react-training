// React
import { ChangeEvent, useEffect, useState } from 'react';

// Enum
import { SIZE } from 'constants/enum';

// Layouts
import GridRow from 'layout/Grid/GridRow/GridRow';
import GridColumn from 'layout/Grid/GridColumn/GridColumn';

// Components
import Toolbar from 'components/Toolbar/Toolbar';
import Table from 'components/Table/Table';
import TableHead from 'components/Table/TableHead/TableHead';

// Sub
import TableBodyRule from './TableBodyRule';
import RuleManagerSub from './RuleManagerSub';

// Hooks
import { useDebounce, useRuleStore, useStore } from 'hooks';

// Action
import { fetchRulesSuccess } from 'actions/rule';

// Services
import { fetchMeta } from 'services/meta';
import { fetchRules } from 'services/rule';
import { fetchRoles } from 'services/role';
import { fetchUsers } from 'services/user';

// Helpers
import { formatDataRules } from 'helpers/formatData';
import { searchName } from 'helpers/search';
import { fetchFailed, fetching, fetchSuccess } from 'actions/loading';
import { IMeta } from 'types/meta';
import { IRole } from 'types/role';
import { IRule } from 'types/rule';
import { IUser } from 'types/users';

const RuleManager = () => {
  const [isSubOpen, setIsSubOpen] = useState(false);
  const { ruleState, dispatchRule } = useRuleStore();
  const { loadingState, dispatchLoading } = useStore();

  const [search, setSearch] = useState('');
  const searchValue = useDebounce(search, 500);
  const results = searchName(ruleState.rules!, searchValue, 'short');

  // Get data from API and set data rules
  const getDataRules = async () => {
    // Set loading
    dispatchLoading(fetching({ ...loadingState }));

    const fetchData = [fetchMeta(), fetchRoles(), fetchUsers(), fetchRules()];

    const data = await Promise.all(fetchData);

    const meta = data[0] as IMeta;
    const roles = data[1] as IRole[];
    const users = data[2] as IUser[];
    const rules = data[3] as IRule[];

    if (rules && meta && roles && users) {
      // Format data
      const ruleFormated = formatDataRules(rules, meta, roles, users);

      // Set actions to fetch success
      dispatchRule(fetchRulesSuccess({ ...ruleState, rules: ruleFormated }));
      dispatchLoading(fetchSuccess({ ...loadingState }));
    } else {
      // Set actions to fetch failed
      dispatchLoading(fetchFailed({ ...loadingState }));
    }
  };

  useEffect(() => {
    getDataRules();
  }, []);

  return (
    <GridRow>
      <GridColumn size={isSubOpen ? SIZE.MEDIUM : SIZE.XLARGE}>
        <Toolbar
          mode='search'
          name='Rules'
          handleChangeSearch={(event: ChangeEvent<HTMLInputElement>) =>
            setSearch(event.target.value)
          }
          setValue={setSearch}
        />
        <Table>
          <TableHead listCell={['Name', 'Description']} />
          <TableBodyRule data={results} setSub={setIsSubOpen} />
        </Table>
      </GridColumn>
      {isSubOpen && (
        <GridColumn size={SIZE.SMALL}>
          <RuleManagerSub />
        </GridColumn>
      )}
    </GridRow>
  );
};

export default RuleManager;
