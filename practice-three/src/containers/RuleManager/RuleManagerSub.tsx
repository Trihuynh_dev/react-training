import { memo } from 'react';

// Hooks
import { useRuleStore } from 'hooks';

// Components
import Toolbar from 'components/Toolbar/Toolbar';

// Layouts
import FormView from 'layout/Form/FormView/FormView';

// Helpers
import { formatDataItem } from 'helpers/formatData';

const RuleManagerSub = () => {
  const { ruleState } = useRuleStore();
  const { ruleSelected } = ruleState;

  return (
    <>
      <Toolbar name={'Rule information'} />
      <FormView
        name={ruleSelected.short}
        title={ruleSelected.long}
        roles={{
          headerList: 'Roles',
          icon: ['fas', 'user-shield'],
          listItem: formatDataItem(ruleSelected.roles, 'id', 'name'),
          highlight: true,
          count: ruleSelected.roles ? ruleSelected.roles.length : 0,
        }}
        users={{
          headerList: 'Users',
          icon: ['fas', 'user-group'],
          listItem: formatDataItem(ruleSelected.users, 'id', 'name'),
          highlight: true,
          count: ruleSelected.users ? ruleSelected.users.length : 0,
        }}
      />
    </>
  );
};

export default memo(RuleManagerSub);
