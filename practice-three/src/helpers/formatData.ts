// Types
import { IRule } from 'types/rule';
import { IItem } from 'types/list';
import { IMeta } from 'types/meta';
import { IRole } from 'types/role';
import { IUser } from 'types/users';

// Helpers
import { findListIdById, findListItemById } from './find';

/**
 * @des Format data for the item
 * @param {T[]} data
 * @param {KP} keyPrimary
 * @param {KS} keySecondary
 * @returns {IItem[]} List item type IItem
 */
const formatDataItem = <T, KP extends keyof T, KS extends keyof T>(
  data: T[] | undefined,
  keyPrimary: KP,
  keySecondary: KS,
): IItem[] => {
  if (!data || data.length === 0) {
    return [];
  }

  const list = data.map((item) => ({
    id: item[keyPrimary] as unknown,
    value: item[keySecondary] as unknown,
  })) as IItem[];

  return list;
};

/**
 * @des Format data for the role (IRole)
 * @param {IRole[]} data
 * @param {IMeta[]} meta
 * @returns {IRole[]} List item type Irole
 */
const formatDataRoles = (data: IRole[], meta: IMeta, rules: IRule[], users: IUser[]): IRole[] => {
  const list = data.map((role) => ({
    ...role,
    rules: findListIdById('rule', 'RoleRule', role.id, meta, rules),
    users: findListIdById('user', 'UserRole', role.id, meta, users),
  }));

  return list;
};

/**
 * @des Format data for the user (IUser)
 * @param {IRole[]} data
 * @param {IMeta[]} meta
 * @returns {IUser[]} List item type IUser
 */
const formatDataUsers = (data: IUser[], meta: IMeta, rules: IRule[], roles: IRole[]): IUser[] => {
  const list = data.map((user) => {
    const listRules = findListIdById('rule', 'UserRule', user.id, meta, rules);
    const listRoles = findListIdById('role', 'UserRole', user.id, meta, roles);
    const allRules = [...listRules];
    const rulesRole = {} as { [key: string]: number[] };

    if (listRoles.length === 0) {
      listRules.forEach((rule) => {
        rulesRole[rule] = [];
      });
    }

    listRoles.forEach((role) => {
      // Find all rule id of role
      const listIdRules = findListIdById('rule', 'RoleRule', role, meta, rules);

      listIdRules.forEach((rule) => {
        // Find all role id of rule
        const listIdRoles = findListIdById('role', 'RoleRule', rule, meta, roles);

        if (!allRules.includes(rule)) {
          allRules.push(rule);
        }

        // Validate role id of role user
        const filterListIdRole = listIdRoles.filter((role) => listRoles.includes(role)).sort();

        // Add to rulesRole
        rulesRole[rule] = filterListIdRole;
      });
    });

    return {
      ...user,
      rules: [...new Set(listRules.sort())],
      roles: listRoles,
      RulesRoles: rulesRole,
      allRules: [...new Set(allRules.sort())],
    };
  });

  return list;
};

/**
 * @des Format data for the rules (IUser)
 * @param {IRule[]} data
 * @param {IMeta[]} meta
 * @param {IRole[]} listAllRole
 * @param {IUser[]} listAllUser
 * @returns {IRule[]} List item type IUser
 */
const formatDataRules = (
  data: IRule[],
  meta: IMeta,
  listAllRole: IRole[],
  listAllUser: IUser[],
): IRule[] => {
  const list = data.map((rule) => {
    const listRole = findListItemById('role', 'RoleRule', rule.id, meta, listAllRole);
    const listUser = findListItemById('user', 'UserRule', rule.id, meta, listAllUser);

    listRole.forEach((role) => {
      const listUserRole = findListItemById('user', 'UserRole', role.id, meta, listAllUser);

      listUserRole.forEach((user) => {
        if (!listUser.includes(user)) {
          listUser.push(user);
        }
      });
    });

    return {
      ...rule,
      roles: listRole,
      users: listUser,
    };
  });

  return list;
};

export { formatDataItem, formatDataRoles, formatDataUsers, formatDataRules };
