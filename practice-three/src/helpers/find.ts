// Type
import { IMeta } from 'types/meta';
import { IRole } from 'types/role';
import { IRule } from 'types/rule';
import { IUser } from 'types/users';

/**
 * @des Find item by id
 * @param {number} id
 * @param {T[]} data
 * @returns {T} Type T or undefined
 */
const findItem = <T extends IUser | IRule | IRole>(id: number, data: T[]): T | null => {
  if (id && data) {
    const item = data.find((item) => item.id === id);
    return item ? item : null;
  }

  return null;
};

/**
 * @des Find list item by id
 * @param {'user' | 'rule' | 'role'} typeId type of item which you want find ('rule' | 'role' | 'user')
 * @param {'UserRole' | 'UserRule' | 'RoleRule'} typeMeta type of scope which you want find
 * @param {number} id id using find item
 * @param {IMeta} meta data of meta
 * @param {T[]} listAllItem all items of type which find
 * @returns {T[]} list item
 */
const findListItemById = <T extends IRule | IRole | IUser>(
  typeId: 'rule' | 'role' | 'user',
  typeMeta: 'UserRole' | 'UserRule' | 'RoleRule',
  id: number,
  meta: IMeta,
  listAllItem: T[],
): T[] => {
  const list: T[] = [];

  let indexCheck = 0;
  let indexData = 1;

  if (
    (typeId === 'role' && typeMeta === 'RoleRule') ||
    (typeId === 'user' && typeMeta === 'UserRole') ||
    (typeId === 'user' && typeMeta === 'UserRule')
  ) {
    indexCheck = 1;
    indexData = 0;
  }

  meta[typeMeta].forEach((item) => {
    if (item[indexCheck] === id) {
      const itemFind = findItem(item[indexData], listAllItem);

      if (itemFind) {
        list.push(itemFind);
      }
    }
  });

  return list;
};

/**
 * @des Find list id by id
 * @param {'user' | 'rule' | 'role'} typeId type of item which you want find ('rule' | 'role' | 'user')
 * @param {'UserRole' | 'UserRule' | 'RoleRule'} typeMeta type of scope which you want find
 * @param {number} id
 * @param {IMeta} data
 * @returns {number[]} list id
 */
const findListIdById = (
  typeId: 'user' | 'rule' | 'role',
  typeMeta: 'UserRole' | 'UserRule' | 'RoleRule',
  id: number,
  data: IMeta,
  listCheck: (IUser | IRule | IRole)[],
): number[] => {
  const list: number[] = [];
  let indexCheck = 0;
  let indexData = 1;

  if (
    (typeId === 'role' && typeMeta === 'RoleRule') ||
    (typeId === 'user' && typeMeta === 'UserRole') ||
    (typeId === 'user' && typeMeta === 'UserRule')
  ) {
    indexCheck = 1;
    indexData = 0;
  }

  data[typeMeta].forEach((item) => {
    const check = listCheck.find((itemCheck) => itemCheck.id === item[indexData]);

    if (item[indexCheck] === id && check !== undefined) {
      list.push(item[indexData]);
    }
  });

  return list;
};

export { findItem, findListItemById, findListIdById };
