const searchName = <T, Key extends keyof T>(data: T[], search: string, key: Key) => {
  if (search === '') {
    return data;
  } else {
    const term = search.toLowerCase();

    return data.filter((item) => {
      const name = item[key] as unknown;

      if (!String(name).toLowerCase().includes(term)) {
        return false;
      }

      return true;
    });
  }
};

export { searchName };
