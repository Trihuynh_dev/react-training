import { createContext } from 'react';

// Reducer
import { initUserState, IUserAction, IUserState } from 'reducers/users';

interface IContext {
  userState: IUserState;
  dispatchUser: React.Dispatch<IUserAction>;
}

const UserContext = createContext<IContext>({
  userState: initUserState,
  dispatchUser: () => { return; },
});

export default UserContext;
