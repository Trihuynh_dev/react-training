// React
import { useReducer } from 'react';

// Context
import UserContext from './UserContext';

// Reducer
import userReducer, { initUserState } from 'reducers/users';

interface IProps {
  children: React.ReactNode;
}

const UserProvider = ({ children }: IProps) => {
  const [userState, dispatchUser] = useReducer(userReducer, initUserState);

  return (
    <UserContext.Provider value={{ userState, dispatchUser }}>{children}</UserContext.Provider>
  );
};

export default UserProvider;
