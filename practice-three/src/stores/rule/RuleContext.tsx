import { createContext } from 'react';

// Reducer
import { initRuleState, IRuleAction, IRuleState } from 'reducers/rules';

interface IContext {
  ruleState: IRuleState;
  dispatchRule: React.Dispatch<IRuleAction>;
}

const RuleContext = createContext<IContext>({
  ruleState: initRuleState,
  dispatchRule: () => { return; },
});

export default RuleContext;
