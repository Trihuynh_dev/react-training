// React
import { useReducer } from 'react';

// Context
import RuleContext from './RuleContext';

// Reducer
import ruleReducer, { initRuleState } from 'reducers/rules';

interface IProps {
  children: React.ReactNode;
}

const RuleProvider = ({ children }: IProps) => {
  const [ruleState, dispatchRule] = useReducer(ruleReducer, initRuleState);

  return (
    <RuleContext.Provider value={{ ruleState, dispatchRule }}>{children}</RuleContext.Provider>
  );
};

export default RuleProvider;
