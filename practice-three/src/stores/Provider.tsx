// React
import { useReducer } from 'react';

// Context
import Context from './Context';

// Reducer
import loadingReducer, { initLoadingState } from 'reducers/loading';

interface IProps {
  children: React.ReactNode;
}

const Provider = ({ children }: IProps) => {
  const [loadingState, dispatchLoading] = useReducer(loadingReducer, initLoadingState);

  return <Context.Provider value={{ loadingState, dispatchLoading }}>{children}</Context.Provider>;
};

export default Provider;
