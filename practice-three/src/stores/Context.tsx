import { createContext } from 'react';

// Reducer
import { ILoadingAction, ILoadingState, initLoadingState } from 'reducers/loading';

interface IContext {
  loadingState: ILoadingState;
  dispatchLoading: React.Dispatch<ILoadingAction>;
}

const Context = createContext<IContext>({
  loadingState: initLoadingState,
  dispatchLoading: () => { return; },
});

export default Context;
