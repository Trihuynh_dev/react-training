import { createContext } from 'react';

// Reducer
import { initRoleState, IRoleAction, IRoleState } from 'reducers/roles';

interface IContext {
  roleState: IRoleState;
  dispatchRole: React.Dispatch<IRoleAction>;
}

const RoleContext = createContext<IContext>({
  roleState: initRoleState,
  dispatchRole: () => { return; },
});

export default RoleContext;
