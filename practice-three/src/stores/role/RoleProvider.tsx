// React
import { useReducer } from 'react';

// Context
import RoleContext from './RoleContext';

// Reducer
import roleReducer, { initRoleState } from 'reducers/roles';

interface IProps {
  children: React.ReactNode;
}

const RoleProvider = ({ children }: IProps) => {
  const [roleState, dispatchRole] = useReducer(roleReducer, initRoleState);

  return (
    <RoleContext.Provider value={{ roleState, dispatchRole }}>{children}</RoleContext.Provider>
  );
};

export default RoleProvider;
