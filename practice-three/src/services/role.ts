// Request (Axios Instance)
import request from './request';

// Constants
import { STATUS_CODE } from 'constants/api';

// Type
import { IRole } from 'types/role';

/**
 * @des Fetch data roles from URL
 * @returns {Promise} Promise<IRole[] | null>
 */
const fetchRoles = async (): Promise<IRole[] | null> => {
  try {
    const res = await request.get(`roles`);

    if (res.status === STATUS_CODE.OK) {
      return res.data;
    }

    return null;
  } catch (error) {
    alert('Failed to fetch roles: ' + error);
    return null;
  }
};

/**
 * @des Creates a new role
 * @param {{name: string}} payload
 * @returns {Promise} Promise
 */
const createRole = async (payload: { name: string }): Promise<{ id: number } | null> => {
  try {
    const res = await request.post('roles', payload);

    if (res.status === STATUS_CODE.OK) {
      return res.data;
    }

    return null;
  } catch (error) {
    alert('Failed to create role: ' + error);
    return null;
  }
};

/**
 * @des Remove role from the list of roles
 * @param {number} id
 * @returns {Promise} Promise<{ id: number } | null>
 */
const removeRole = async (id: number): Promise<{ id: number } | null> => {
  try {
    const res = await request.delete(`roles/${id}`);

    if (res.status === STATUS_CODE.OK) {
      return res.data;
    }

    return null;
  } catch (error) {
    alert('Failed to delete role: ' + error);
    return null;
  }
};

/**
 * @des Update role from server
 * @param {(string | number)} id
 * @param {IRule} payload
 * @returns {Promise} Promise<{ id: number } | null>
 */
const updateRole = async (id: string | number, payload: IRole): Promise<{ id: number } | null> => {
  try {
    const res = await request.put(`roles/${id}`, payload);

    if (res.status === STATUS_CODE.OK) {
      return res.data;
    }

    return null;
  } catch (error) {
    alert('Failed to update role:' + error);
    return null;
  }
};

export { fetchRoles, createRole, removeRole, updateRole };
