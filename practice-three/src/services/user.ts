// Request (Axios Instance)
import request from './request';

// Constants
import { STATUS_CODE } from 'constants/api';

// Type
import { IFileImage } from 'types/fileImage';
import { IUser } from 'types/users';

/**
 * @des Fetch data user from URL
 * @returns {Promise} Promise<IUser[] | null>
 */
const fetchUsers = async (): Promise<IUser[] | null> => {
  try {
    const res = await request.get(`users`);

    if (res.status === STATUS_CODE.OK) {
      return res.data;
    }

    return null;
  } catch (error) {
    alert('Failed to fetch users: ' + error);
    return null;
  }
};

/**
 * @des Creates a new user
 * @param {{ name: string }} payload
 * @returns {Promise} Promise
 */
const createUser = async (payload: { name: string }): Promise<{ id: number } | null> => {
  try {
    const res = await request.post('users', payload);

    if (res.status === STATUS_CODE.OK) {
      return res.data;
    }

    return null;
  } catch (error) {
    alert('Failed to create user: ' + error);
    return null;
  }
};

/**
 * @des Remove user from the list of users
 * @param {number} id
 * @returns {Promise}
 */
const removeUser = async (id: number): Promise<{ id: number } | null> => {
  try {
    const res = await request.delete(`users/${id}`);

    if (res.status === STATUS_CODE.OK) {
      return res.data;
    }

    return null;
  } catch (error) {
    alert('Failed to delete user: ' + error);
    return null;
  }
};

/**
 * @des Update user profile from server
 * @param {(string | number)} id
 * @param {Object} payload
 * @returns {Promise}
 */
const updateUser = async (id: string | number, payload: IUser): Promise<{ id: number } | null> => {
  try {
    const res = await request.put(`users/${id}`, payload);

    if (res.status === STATUS_CODE.OK) {
      return res.data;
    }

    return null;
  } catch (error) {
    alert('Failed to update user:' + error);
    return null;
  }
};

/**
 * @des Upload avatar image
 * @param {(string | number)} id
 * @param {FormData} payload {upload: file, upload_fullpath: file.name}
 * @returns {Promise}
 */
const uploadAvatar = async (id: string | number, payload: object): Promise<IFileImage | null> => {
  try {
    const res = await request.post(`users/${id}/avatar`, payload);

    if (res.status === STATUS_CODE.OK) {
      return res.data;
    }

    return null;
  } catch (error) {
    alert('Failed to upload avatar: ' + error);
    return null;
  }
};

export { createUser, fetchUsers, removeUser, updateUser, uploadAvatar };
