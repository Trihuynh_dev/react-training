// Request (Axios Instance)
import request from './request';

// Constants
import { STATUS_CODE } from 'constants/api';

// Type
import { IMeta } from 'types/meta';

const fetchMeta = async (): Promise<IMeta | null> => {
  try {
    const res = await request.get(`meta`);

    if (res.status === STATUS_CODE.OK) {
      return res.data;
    }

    return null;
  } catch (error) {
    alert('Failed to fetch meta: ' + error);
    return null;
  }
};

export { fetchMeta };
