import axios from 'axios';

// Constants
import { BASE_URL_API } from 'constants/api';

const request = axios.create({
  baseURL: BASE_URL_API,
  timeout: 10000,
});

export default request;
