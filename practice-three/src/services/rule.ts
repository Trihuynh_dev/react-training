// Request (Axios Instance)
import request from './request';

// Constants
import { STATUS_CODE } from 'constants/api';

// Type
import { IRule } from 'types/rule';

/**
 * @des Fetch data rules from URL
 * @returns {Promise} Promise<IRule[] | null>
 */
const fetchRules = async (): Promise<IRule[] | null> => {
  try {
    const res = await request.get(`rules`);

    if (res.status === STATUS_CODE.OK) {
      return res.data;
    }

    return null;
  } catch (error) {
    alert('Failed to fetch rules: ' + error);
    return null;
  }
};

export { fetchRules };
