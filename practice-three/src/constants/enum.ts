enum SIZE {
  XSMALL = 'xsmall',
  SMALL = 'small',
  MEDIUM = 'medium',
  LARGE = 'large',
  XLARGE = 'xlarge',
}

enum ORDINAL_NUMBER {
  PRIMARY = 'primary',
  SECONDARY = 'secondary',
  TERTIARY = 'tertiary',
}

enum STATUS {
  ACTIVE = 'Active',
  NOT_ACTIVE = 'Not active',
}

export { SIZE, ORDINAL_NUMBER, STATUS };
