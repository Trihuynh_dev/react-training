// Types
import { IRoutes } from 'types/routes';

// Containers
import UserManager from 'containers/UserManager';
import RoleManager from 'containers/RoleManager';
import RuleManager from 'containers/RuleManager';

// Stores
import UserProvider from 'stores/user/UserProvider';
import RoleProvider from 'stores/role/RoleProvider';
import RuleProvider from 'stores/rule/RuleProvider';

// Public routes
const publicRoutes: IRoutes[] = [
  {
    name: 'Users',
    path: '/',
    icon: ['fas', 'user-group'],
    component: UserManager,
    provider: UserProvider,
  },
  {
    name: 'Roles',
    path: '/role',
    icon: ['fas', 'user-shield'],
    component: RoleManager,
    provider: RoleProvider,
  },
  {
    name: 'Rules',
    path: '/rule',
    icon: ['fas', 'list-check'],
    component: RuleManager,
    provider: RuleProvider,
  },
];

export { publicRoutes };
