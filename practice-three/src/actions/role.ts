// Types action
import { FETCH_ROLE } from 'constants/actionType';

// Reducer
import { IRoleState } from 'reducers/roles';

// Fetch role success
const fetchRolesSuccess = (payload: IRoleState) => ({
  type: FETCH_ROLE.SUCCESS,
  payload,
});

// Set role selected
const setRoleSelected = (payload: IRoleState) => ({
  type: FETCH_ROLE.SET_ROLE_SELECTED,
  payload,
});

// Set role selected
const updateRoleSuccess = (payload: IRoleState) => ({
  type: FETCH_ROLE.UPDATE_SUCCESS,
  payload,
});

const deleteRoleSuccess = (payload: IRoleState) => ({
  type: FETCH_ROLE.DELETE_SUCCESS,
  payload,
});

export { fetchRolesSuccess, setRoleSelected, updateRoleSuccess, deleteRoleSuccess };
