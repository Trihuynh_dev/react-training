// Reducers
import { ILoadingState } from 'reducers/loading';

// Action types
import { FETCH } from 'constants/actionType';

// Fetching rules
const fetching = (payload: ILoadingState) => ({
  type: FETCH.FETCHING,
  payload,
});

// Fetching failed
const fetchFailed = (payload: ILoadingState) => ({
  type: FETCH.FAILED,
  payload,
});

// Fetch success
const fetchSuccess = (payload: ILoadingState) => ({
  type: FETCH.SUCCESS,
  payload,
});

export { fetching, fetchSuccess, fetchFailed };
