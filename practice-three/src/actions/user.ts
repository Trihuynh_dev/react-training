// Types action
import { FETCH_USER } from 'constants/actionType';

// Types
import { IUserState } from 'reducers/users';

// Fetch users success
const fetchUsersSuccess = (payload: IUserState) => ({
  type: FETCH_USER.SUCCESS,
  payload,
});

// Set user selected
const setUserSelected = (payload: IUserState) => ({
  type: FETCH_USER.SET_USER_SELECTED,
  payload,
});

// Update user success
const updateUserSuccess = (payload: IUserState) => ({
  type: FETCH_USER.UPDATE_SUCCESS,
  payload,
});

// Delete user success
const deleteUserSuccess = (payload: IUserState) => ({
  type: FETCH_USER.DELETE_SUCCESS,
  payload,
});

export { fetchUsersSuccess, setUserSelected, updateUserSuccess, deleteUserSuccess };
