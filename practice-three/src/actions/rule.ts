// Types action
import { FETCH_RULE } from 'constants/actionType';

// Types
import { IRuleState } from 'reducers/rules';

// Fetch rules success
const fetchRulesSuccess = (payload: IRuleState) => ({
  type: FETCH_RULE.SUCCESS,
  payload,
});

// Set rule selected
const setRuleSelected = (payload: IRuleState) => ({
  type: FETCH_RULE.SET_RULE_SELECTED,
  payload,
});

export { fetchRulesSuccess, setRuleSelected };
