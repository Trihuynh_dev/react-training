// Types action
import { FETCH } from 'constants/actionType';

interface ILoadingState {
  isLoading: boolean;
  statusLoading: boolean;
}

interface ILoadingAction {
  type: string;
  payload: ILoadingState;
}

const initLoadingState: ILoadingState = {
  isLoading: true,
  statusLoading: false,
};

const loadingReducer = (state: ILoadingState, action: ILoadingAction) => {
  switch (action.type) {
    case FETCH.FETCHING:
      return {
        ...state,
        isLoading: true,
      };

    case FETCH.SUCCESS:
      return {
        isLoading: false,
        statusLoading: true,
      };

    case FETCH.FAILED:
      return {
        ...state,
        isLoading: false,
        statusLoading: false,
      };

    // Case default
    default:
      return state;
  }
};

export { initLoadingState };
export type { ILoadingAction, ILoadingState };
export default loadingReducer;
