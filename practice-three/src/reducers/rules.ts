// Types action
import { FETCH_RULE } from 'constants/actionType';

// Types
import { IRule } from 'types/rule';

interface IRuleState {
  rules: IRule[] | null;
  ruleSelected: IRule;
}

interface IRuleAction {
  type: string;
  payload: IRuleState;
}

const initRuleState: IRuleState = {
  rules: null,
  ruleSelected: {} as IRule,
};

const ruleReducer = (state: IRuleState, action: IRuleAction) => {
  switch (action.type) {
    case FETCH_RULE.SUCCESS:
      return {
        ...state,
        rules: action.payload.rules,
      };

    case FETCH_RULE.SET_RULE_SELECTED:
      return {
        ...state,
        ruleSelected: action.payload.ruleSelected,
      };
    // Case default
    default:
      return state;
  }
};

export { initRuleState };
export type { IRuleState, IRuleAction };
export default ruleReducer;
