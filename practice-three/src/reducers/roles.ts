// Types action
import { FETCH_ROLE } from 'constants/actionType';

// Types
import { IRole } from 'types/role';

interface IRoleState {
  roles: IRole[] | null;
  roleSelected: IRole;
}

interface IRoleAction {
  type: string;
  payload: IRoleState;
}

const initRoleState: IRoleState = {
  roles: null,
  roleSelected: {} as IRole,
};

const roleReducer = (state: IRoleState, action: IRoleAction) => {
  switch (action.type) {
    case FETCH_ROLE.SUCCESS:
      return {
        ...state,
        roles: action.payload.roles,
      };

    case FETCH_ROLE.SET_ROLE_SELECTED:
      return {
        ...state,
        roleSelected: action.payload.roleSelected,
      };

    case FETCH_ROLE.UPDATE_SUCCESS:
      return {
        roles: action.payload.roles,
        roleSelected: action.payload.roleSelected,
      };

    case FETCH_ROLE.DELETE_SUCCESS:
      return {
        ...state,
        roles: action.payload.roles,
      };
    // Case default
    default:
      return state;
  }
};

export { initRoleState };
export type { IRoleState, IRoleAction };
export default roleReducer;
