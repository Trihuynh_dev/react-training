// Types action
import { FETCH_USER } from 'constants/actionType';

// Types
import { IUser } from 'types/users';

interface IUserState {
  users: IUser[] | null;
  userSelected: IUser;
}

interface IUserAction {
  type: string;
  payload: IUserState;
}

const initUserState: IUserState = {
  users: null,
  userSelected: {} as IUser,
};

const userReducer = (state: IUserState, action: IUserAction) => {
  switch (action.type) {
    case FETCH_USER.SUCCESS:
      return {
        ...state,
        users: action.payload.users,
      };

    case FETCH_USER.SET_USER_SELECTED:
      return {
        ...state,
        userSelected: action.payload.userSelected,
      };

    case FETCH_USER.UPDATE_SUCCESS:
      return {
        userSelected: action.payload.userSelected,
        users: action.payload.users,
      };

    case FETCH_USER.DELETE_SUCCESS:
      return {
        ...state,
        users: action.payload.users,
      };
    // Case default
    default:
      return state;
  }
};

export { initUserState };
export type { IUserState, IUserAction };
export default userReducer;
