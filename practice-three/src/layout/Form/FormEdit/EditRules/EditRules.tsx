// React
import { ChangeEvent, memo, useCallback, useEffect, useState } from 'react';

// Clsx
import clsx from 'clsx';

// Hooks
import { useDebounce } from 'hooks';

// Type
import { IRule } from 'types/rule';
import { IRole } from 'types/role';

// Enum
import { SIZE } from 'constants/enum';

// Components
import Button from 'components/Button/NormalButton/Button';
import RadioButton from 'components/RadioButton/RadioButton';
import Marker from 'components/Marker/Marker';
import Checkbox from 'components/Checkbox/Checkbox';

// Helpers
import { searchName } from 'helpers/search';

// Styles
import './editRules.scss';

interface RulesRoles {
  id: number;
  short: string;
  long: string;
  roles: (IRole | undefined)[];
  assignDirect: boolean;
}

interface IProps {
  name: string;
  listAllRules: IRule[];
  listDirectRules: IRule[];
  allRulesAssigned?: (IRule | null)[] | null;
  hasCheckbox?: boolean;
  roles?: IRole[];
  rulesRole?: { [key: string]: number[] };
  handleChange: (id: string, checked: boolean) => void;
}

const EditRules = (props: IProps) => {
  const [isDirectRules, setIsDirectRules] = useState(true);
  const [isBtnDisabled, setIsBtnDisabled] = useState(false);
  const [btnName, setBtnName] = useState('Modify');
  const [isEditOpened, setIsEditOpened] = useState(false);
  const [search, setSearch] = useState('');
  const searchValue = useDebounce(search, 500);

  const {
    name,
    listAllRules,
    listDirectRules,
    allRulesAssigned,
    hasCheckbox,
    rulesRole,
    handleChange,
    roles,
  } = props;

  // List search direct rules
  const resultsSearchAssignDirect = searchName(listDirectRules, searchValue, 'short');

  // List search edit rules
  const resultsSearchAllRules = searchName(listAllRules, searchValue, 'short');

  useEffect(() => {
    setSearch('');
  }, [isEditOpened, isDirectRules]);

  // Set on/off edit mode
  const editRule = () => {
    setIsEditOpened(!isEditOpened);
    btnName === 'Modify' ? setBtnName('Done') : setBtnName('Modify');
  };

  // Open list all rules
  const checkIsListRuleRole = useCallback(() => {
    setIsDirectRules(false);
    setIsBtnDisabled(true);
    setIsEditOpened(false);
    setBtnName('Modify');
  }, []);

  // Open list direct rules
  const checkIsDirectRules = useCallback(() => {
    setIsDirectRules(true);
    setIsBtnDisabled(false);
  }, []);

  // Render edit rules
  const renderEditRule = () => {
    return resultsSearchAllRules.map((rule) => {
      const checked = listDirectRules.includes(rule);

      return (
        <div
          key={rule.id}
          className={clsx('edit-rules__item', { 'edit-rules__item--checked': checked })}
        >
          <Checkbox id={rule.id.toString()} checked={checked} handleCheck={handleChange}>
            <p className='edit-rules__item__title'>{rule.short}</p>
            <p>{rule.long}</p>
          </Checkbox>
        </div>
      );
    });
  };

  // Render list direct rules
  const renderListDirectRules = () => {
    return resultsSearchAssignDirect.map((rule) => {
      if (rule) {
        return (
          <div key={rule.id} className='edit-rules__item edit-rules__item--checked'>
            <p className='edit-rules__item__title'>{rule.short}</p>
            <p>{rule.long}</p>
          </div>
        );
      }
    });
  };

  const arrayRulesRoles = (): RulesRoles[] => {
    if (!rulesRole) {
      return [];
    }

    const arrayRulesRoles = Object.entries(rulesRole!);

    return arrayRulesRoles.map((item) => {
      const rule = listAllRules.find((rule) => rule!.id === Number(item[0]));
      let ruleSearch = {} as RulesRoles;
      if (rule) {
        ruleSearch = {
          ...rule,
          assignDirect: listDirectRules.includes(rule),
          roles: item[1].map((item) => roles!.find((role) => role.id === item)),
        };
      }
      return ruleSearch;
    });
  };

  // List search rule role
  const resultsSearchRulesRole = searchName(arrayRulesRoles(), searchValue, 'short');

  // Render list rule with role
  const renderListRuleRole = () => {
    return resultsSearchRulesRole.map((rule) => (
      <div key={rule.id} className='edit-rules__item'>
        <div className='d-flex'>
          <p className='edit-rules__item__title'>{rule.short}:</p>
          <p>&nbsp;{rule.long.toLowerCase()}</p>
        </div>
        <div className='d-flex'>
          {rule.assignDirect && <Marker name='Assigned directly' disable={true} />}
          {rule.roles.map((role) => {
            if (role)
              return (
                <Marker key={role.id} name={role.name} color={role.color} disable={false} icon />
              );
          })}
        </div>
      </div>
    ));
  };

  return (
    <>
      <div className='edit-rules__control'>
        <p className='edit-rules__label'>{name}</p>
        <div className='d-flex d-flex--space-between'>
          {!hasCheckbox ? (
            listDirectRules!.length === 0 ? (
              <p>{'No rules assigned'}</p>
            ) : (
              <p>{`Rules assigned (${listDirectRules!.length})`}</p>
            )
          ) : (
            <div>
              <RadioButton
                label='Assigned directly'
                htmlFor={''}
                checked={isDirectRules}
                count={listDirectRules ? listDirectRules.length : 0}
                onChange={checkIsDirectRules}
              />
              <RadioButton
                label='All assignments'
                htmlFor={''}
                checked={!isDirectRules}
                count={allRulesAssigned ? allRulesAssigned.length : 0}
                onChange={checkIsListRuleRole}
              />
            </div>
          )}
          <Button size={SIZE.SMALL} label={btnName} onClick={editRule} disabled={isBtnDisabled} />
        </div>
      </div>
      <div className='edit-rules__search'>
        <input
          className='edit-rules__input'
          value={search}
          onChange={(event: ChangeEvent<HTMLInputElement>) => setSearch(event.target.value)}
          placeholder='Search'
        />
      </div>
      <div className='edit-rules__list'>
        {isEditOpened
          ? renderEditRule()
          : isDirectRules
          ? renderListDirectRules()
          : renderListRuleRole()}
      </div>
    </>
  );
};

export default memo(EditRules);
