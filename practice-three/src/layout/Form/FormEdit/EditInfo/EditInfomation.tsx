// React
import { memo } from 'react';

// Enum
import { SIZE } from 'constants/enum';

// Components
import Button from 'components/Button/NormalButton/Button';

interface IProps {
  children: React.ReactNode;
  nameBtnPrimary: string;
  handleBtnPrimary: () => void;
  nameBtnSecondary: string;
  handleBtnSecondary: () => void;
}

const EditInfomation = (props: IProps) => {
  const {
    children,
    nameBtnPrimary,
    handleBtnPrimary,
    nameBtnSecondary,
    handleBtnSecondary
  } = props;

  return (
    <div className='padding-m'>
      <div className='align-right'>
        <Button size={SIZE.SMALL} label={nameBtnPrimary} onClick={handleBtnPrimary} />
        <Button
          size={SIZE.SMALL}
          label={nameBtnSecondary}
          primary={true}
          onClick={handleBtnSecondary}
        />
      </div>
      <div className='mg-top-m'>{children}</div>
    </div>
  );
};

export default memo(EditInfomation);
