// React
import { ChangeEvent, memo, useEffect, useState } from 'react';

// Clsx
import clsx from 'clsx';

// Hooks
import { useDebounce } from 'hooks';

// Type
import { IRole } from 'types/role';
import { IUser } from 'types/users';

// Enum
import { SIZE } from 'constants/enum';

// Components
import Button from 'components/Button/NormalButton/Button';
import Checkbox from 'components/Checkbox/Checkbox';
import Avatar from 'components/Avatar/Avatar';

// Styles
import './editCheckbox.scss';

// Helpers
import { searchName } from 'helpers/search';

interface IProps {
  name: string;
  type: string;
  listAll: Partial<IRole>[] | Partial<IUser>[];
  listAssign: Partial<IRole>[] | Partial<IUser>[];
  handleChange: (id: string, checked: boolean) => void;
}

const EditRules = (props: IProps) => {
  const [btnName, setBtnName] = useState('Modify');
  const [isEditOpened, setIsEditOpened] = useState(false);
  const [search, setSearch] = useState('');
  const valueSearch = useDebounce(search, 500);

  const { name, listAll, listAssign, type, handleChange } = props;

  useEffect(() => {
    setSearch('');
  }, [isEditOpened]);

  // Result search list assigned
  const resultsSearchListAssigned = searchName(listAssign, valueSearch, 'name') as
    | IRole[]
    | IUser[];

  // Result search list all
  const resultsSearchListAll = searchName(listAll, valueSearch, 'name') as IRole[] | IUser[];

  // Set state for edit
  const setEditState = () => {
    setIsEditOpened(!isEditOpened);
    btnName === 'Modify' ? setBtnName('Done') : setBtnName('Modify');
  };

  // Render list edit with checkbox
  const renderEdit = () => {
    return resultsSearchListAll.map((item) => {
      let avatar: string | undefined;
      let color: string | undefined;

      if ('avatar' in item) {
        avatar = item.avatar;
      }

      if ('color' in item) {
        color = item.color;
      }

      const checked = listAssign.includes(item);

      return (
        <div
          key={item.id}
          className={clsx('edit-checkbox__item', { 'edit-checkbox__item--checked': checked })}
        >
          <Checkbox id={item.id!.toString()} checked={checked} handleCheck={handleChange}>
            <div className='d-flex'>
              <Avatar name={item.name!} size={SIZE.SMALL} circle avatar={avatar} color={color} />
              <p className='edit-checkbox__item__label'>{item.name}</p>
            </div>
          </Checkbox>
        </div>
      );
    });
  };

  // Render list assignments
  const renderListAssign = () => {
    return resultsSearchListAssigned.map((item) => {
      let avatar: string | undefined;
      let color: string | undefined;

      if ('avatar' in item) {
        avatar = item.avatar;
      }

      if ('color' in item) {
        color = item.color;
      }

      return (
        <div key={item.id} className='edit-checkbox__item edit-checkbox__item--checked'>
          <Avatar name={item.name!} size={SIZE.SMALL} circle avatar={avatar} color={color} />
          <p className='edit-checkbox__item__label'>{item.name}</p>
        </div>
      );
    });
  };

  return (
    <>
      <div className='edit-rules__control'>
        <p className='edit-rules__label'>{name}</p>
        <div className='d-flex d-flex--space-between'>
          <div>
            {listAssign.length === 0 ? (
              <p>{`No ${type.toLowerCase()} assigned`}</p>
            ) : (
              <p>{`${type} assigined (${listAssign.length})`}</p>
            )}
          </div>
          <Button size={SIZE.SMALL} label={btnName} onClick={setEditState} />
        </div>
      </div>
      <div className='edit-rules__search'>
      <input
          className='edit-rules__input'
          value={search}
          onChange={(event: ChangeEvent<HTMLInputElement>) => setSearch(event.target.value)}
          placeholder='Search'
        />
      </div>
      <div className='edit-rules__list'>{isEditOpened ? renderEdit() : renderListAssign()}</div>
    </>
  );
};

export default memo(EditRules);
