import { memo } from 'react';

// Clsx
import clsx from 'clsx';

// Type
import { IListItem } from 'types/list';

//Component
import Card from 'components/Card/Card';
import List from 'components/List/List';

// Styles
import './formView.scss';

interface IProps {
  name: string;
  avatar?: string;
  hasAvatar?: boolean;
  color?: string;
  title?: string;
  email?: IListItem;
  lastVisited?: IListItem;
  users?: IListItem;
  roles?: IListItem;
  rules?: IListItem;
}

const FormView = (props: IProps) => {
  const { rules, roles, users, hasAvatar, name, color, title, email, lastVisited, avatar } = props;

  return (
    <div className='information'>
      <Card hasAvatar={hasAvatar!} avatar={avatar!} name={name} title={title!} color={color!} />
      <div className={clsx('information__list', { 'information__list--no-avatar': !hasAvatar })}>
        {email && <List {...email} />}
        {lastVisited && <List {...lastVisited} />}
        {roles && <List {...roles} />}
        {rules && <List {...rules} />}
        {users && <List {...users} />}
      </div>
    </div>
  );
};

export default memo(FormView);
