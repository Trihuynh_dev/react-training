import { ComponentMeta, ComponentStory } from '@storybook/react';

// Type
import { IListItem } from 'types/list';

// Components
import FormView from './FormView';

const listItem: IListItem['listItem'] = [
  {
    id: '1',
    value: 'Item 1',
  },
  {
    id: '2',
    value: 'Item 2',
  },
  {
    id: '3',
    value: 'Item 2',
  },
];

const listItemHightlight: IListItem['listItem'] = [
  {
    id: '1',
    value: 'Item 1',
  },
  {
    id: '2',
    value: 'Item 2',
  },
  {
    id: '3',
    value: 'Item 2',
  },
];

export default {
  title: 'Layout/FormView',
  component: FormView,
} as ComponentMeta<typeof FormView>;

const Template: ComponentStory<typeof FormView> = (args) => <FormView {...args} />;

const Default = Template.bind({});
Default.args = {
  name: 'Name',
  hasAvatar: true,
  email: {
    headerList: 'List',
    icon: ['fas', 'envelope'],
    listItem: listItem,
    count: listItem.length,
  },
  roles: {
    headerList: 'List',
    icon: ['fas', 'envelope'],
    listItem: listItemHightlight,
    count: listItemHightlight.length,
    highlight: true,
  },
};

export { Default };
