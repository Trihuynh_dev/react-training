import { ReactNode, useEffect, useRef, useState } from 'react';

// React router dom
import { useNavigate } from 'react-router-dom';

// Hooks
import { useRoleStore, useStore, useUserStore } from 'hooks';

// Enums
import { SIZE } from 'constants/enum';

// Components
import Button from 'components/Button/NormalButton/Button';
import Header from 'components/Header/Header';
import Modal from 'components/Modal/Modal';
import SidebarList from 'components/Sidebar/ListItem/SidebarList';
import Sidebar from 'components/Sidebar/Sidebar';

// Layouts
import Grid from 'layout/Grid/Grid';
import GridColumn from 'layout/Grid/GridColumn/GridColumn';
import GridRow from 'layout/Grid/GridRow/GridRow';
import Popover from 'components/Popover/Popover';
import Loading from 'components/Loading/Loading';

// Actions
import { updateUserSuccess } from 'actions/user';
import { updateRoleSuccess } from 'actions/role';
import { fetchFailed, fetching, fetchSuccess } from 'actions/loading';

// Services
import { createUser } from 'services/user';
import { createRole } from 'services/role';

// Routes
import { publicRoutes } from 'routes';

// Styles
import './defaultLayout.scss';

interface IProps {
  children: ReactNode;
}

const DefaultLayout = ({ children }: IProps) => {
  const [isRenderLoading, setIsRenderLoading] = useState(false);
  const [stateUserModal, setStateUserModal] = useState(false);
  const [stateRoleModal, setStateRoleModal] = useState(false);
  const [isPopoverOpened, setIsPopoverOpened] = useState(false);

  // useRef
  const inputNameUserRef = useRef<HTMLInputElement>(null);
  const inputNameRoleRef = useRef<HTMLInputElement>(null);

  // useNavigate
  const navigate = useNavigate();

  // Stores
  const { loadingState, dispatchLoading } = useStore();
  const { userState, dispatchUser } = useUserStore();
  const { roleState, dispatchRole } = useRoleStore();

  const listItemPopover = [
    {
      name: 'Add new user',
      onClick: () => {
        setStateUserModal(true);
        setIsPopoverOpened(false);
      },
    },
    {
      name: 'Add role user',
      onClick: () => {
        setStateRoleModal(true);
        setIsPopoverOpened(false);
      },
    },
  ];

  useEffect(() => {
    if (inputNameRoleRef.current) {
      inputNameRoleRef.current.focus();
    }
  }, [stateRoleModal]);

  useEffect(() => {
    if (inputNameUserRef.current) {
      inputNameUserRef.current.focus();
    }
  }, [stateUserModal]);

  useEffect(() => {
    setIsRenderLoading(true);

    const handler = setTimeout(() => {
      setIsRenderLoading(false);
    }, 2000);

    return () => {
      clearTimeout(handler);
    };
  }, [loadingState.isLoading, loadingState.statusLoading]);

  // Handle add new user
  const handleAddUser = async () => {
    if (inputNameUserRef.current) {
      if (inputNameUserRef.current.value === '') {
        alert('Please enter a name');
      } else {
        // Navigate to /
        navigate('/');

        setStateUserModal(false);

        //Set loading
        dispatchLoading(fetching({ ...loadingState }));

        const name = inputNameUserRef.current.value;
        const res = await createUser({ name: name });

        if (res) {
          const userAdd = {
            id: res.id,
            name: name,
            email: '',
            avatar: '',
            status: 0,
            roles: [],
            allRules: [],
            rules: [],
            RulesRoles: {},
          };

          const usersUpdate = userState.users;
          if (usersUpdate) {
            usersUpdate.push(userAdd);
          }

          // Success
          dispatchLoading(fetchSuccess({ ...loadingState }));

          dispatchUser(
            updateUserSuccess({
              ...userState,
              users: usersUpdate,
            }),
          );
        } else {
          // Failed
          dispatchLoading(fetchFailed({ ...loadingState }));
        }
      }
    }
  };

  // Handle add new role
  const handleAddRole = async () => {
    if (inputNameRoleRef.current) {
      if (inputNameRoleRef.current.value === '') {
        alert('Please enter a name');
      } else {
        // navigate to /role
        navigate('/role');

        setStateRoleModal(false);

        //Set loading
        dispatchLoading(fetching({ ...loadingState }));

        const name = inputNameRoleRef.current.value;
        const res = await createRole({ name: name });

        if (res) {
          const roleAdd = {
            id: res.id,
            name: name,
            detail: '',
            color: '',
            rules: [],
            users: [],
          };

          const rolesUpdate = roleState.roles;
          if (rolesUpdate) {
            rolesUpdate.push(roleAdd);
          }

          // Success
          dispatchLoading(fetchSuccess({ ...loadingState }));

          dispatchRole(
            updateRoleSuccess({
              ...roleState,
              roles: rolesUpdate,
            }),
          );
        } else {
          // Failed
          dispatchLoading(fetchFailed({ ...loadingState }));
        }
      }
    }
  };

  return (
    <>
      <Header>
        <h1 className='header__branch'>Users Manager</h1>
        {isRenderLoading && (
          <Loading isLoading={loadingState.isLoading} status={loadingState.statusLoading} />
        )}
      </Header>
      <Grid>
        <GridRow>
          <GridColumn size={SIZE.XSMALL}>
            <Sidebar>
              <div className='d-flex-center-col'>
                <Button
                  label='+ New'
                  primary={true}
                  size={SIZE.LARGE}
                  onClick={() => setIsPopoverOpened(!isPopoverOpened)}
                />
                <div className='sidebar__popover'>
                  {isPopoverOpened && <Popover listItem={listItemPopover} />}
                </div>
              </div>
              <SidebarList listItem={publicRoutes} />
            </Sidebar>
          </GridColumn>
          <GridColumn size={SIZE.LARGE}>
            <div className='main'>{children}</div>
          </GridColumn>
        </GridRow>
      </Grid>
      <Modal
        ref={inputNameUserRef}
        stateModal={stateUserModal}
        title='Enter user name'
        btnNamePrimary='Save'
        onClickBtnPrimary={handleAddUser}
        hasInput={true}
        handleCloseModal={() => setStateUserModal(false)}
      />
      <Modal
        ref={inputNameRoleRef}
        stateModal={stateRoleModal}
        title='Enter role name'
        btnNamePrimary='Save'
        onClickBtnPrimary={handleAddRole}
        hasInput={true}
        handleCloseModal={() => setStateRoleModal(false)}
      />
    </>
  );
};

export default DefaultLayout;
