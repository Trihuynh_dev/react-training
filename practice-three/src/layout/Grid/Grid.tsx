import React from 'react';

// Styles
import './grid.scss';

interface IProps {
  children?: React.ReactNode | React.ReactNode[];
}

const Grid = (props: IProps) => {
  const { children } = props;

  return <div className='grid'>{children}</div>;
};

export default Grid;
