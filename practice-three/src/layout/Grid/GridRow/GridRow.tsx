import React from 'react';

// Styles
import './gridRow.scss';

interface IProps {
  children?: React.ReactNode;
}

const GridRow = (props: IProps) => {
  const { children } = props;

  return <div className='grid__row'>{children}</div>;
};

export default GridRow;
