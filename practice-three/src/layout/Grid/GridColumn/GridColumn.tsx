import React from 'react';

// Enum
import { SIZE } from 'constants/enum';

// Styles
import './gridColumn.scss';

interface IProps {
  size: SIZE;
  children?: React.ReactNode | React.ReactNode[];
}

const GridColumn = (props: IProps) => {
  const { size, children } = props;

  return <div className={`grid__column grid__column--${size}`}>{children}</div>;
};

export default GridColumn;
