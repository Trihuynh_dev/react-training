import clsx from 'clsx';

// Enum
import { SIZE } from 'constants/enum';

// Styles
import './button.scss';

interface IProps {
  primary?: boolean;
  size: SIZE.SMALL | SIZE.MEDIUM | SIZE.LARGE;
  label: string;
  onClick?: () => void;
  disabled?: boolean;
}

const Button = (props: IProps) => {
  const { primary, size, label, onClick, disabled } = props;

  return (
    <button
      type='button'
      className={clsx('btn', `btn--${size}`, { 'btn--primary': primary })}
      onClick={onClick}
      disabled={disabled}
    >
      {label}
    </button>
  );
};

export default Button;
