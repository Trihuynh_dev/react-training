import { Dispatch, SetStateAction, memo } from 'react';

// Styless
import './switchButton.scss';

interface IProps {
  checked: boolean;
  handleClick: Dispatch<SetStateAction<boolean>>;
}

const SwitchButton = (props: IProps) => {
  const { checked, handleClick } = props;

  return (
    <div className='switch'>
      <label>
        <input type='checkbox' checked={checked} onChange={() => handleClick(!checked)} />
        <span className='slider round'></span>
      </label>
    </div>
  );
};

export default memo(SwitchButton);
