import { ComponentMeta, ComponentStory } from '@storybook/react';

// Components
import Popover from './Popover';

export default {
  title: 'Popover',
  componnet: Popover,
} as ComponentMeta<typeof Popover>;

const listItemPopover = [
  {
    name: 'Action 1',
    onClick: () => alert('Action 1'),
  },
  {
    name: 'Action 2',
    onClick: () => alert('Action 2'),
  },
];

const Template: ComponentStory<typeof Popover> = (args) => <Popover {...args} />;

const Default = Template.bind({});
Default.args = {
  listItem: listItemPopover,
};

export { Default };
