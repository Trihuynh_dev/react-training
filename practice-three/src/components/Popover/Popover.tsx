// Styles
import './popover.scss';

interface IProps {
  listItem: { name: string; onClick: () => void }[];
}

const Popover = ({ listItem }: IProps) => {
  return (
    <ul className='popover'>
      {listItem.map((item, index) => (
        <li className='popover__item' key={index} onClick={item.onClick} aria-hidden>
          {item.name}
        </li>
      ))}
    </ul>
  );
};

export default Popover;
