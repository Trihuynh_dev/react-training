import { ComponentMeta, ComponentStory } from '@storybook/react';

import Card from './Card';

export default {
  title: 'Card',
  component: Card,
} as ComponentMeta<typeof Card>;

const Template: ComponentStory<typeof Card> = (args) => <Card {...args} />;

const Default = Template.bind({});
Default.args = {
  name: 'Name',
};

const HasTitle = Template.bind({});
HasTitle.args = {
  name: 'Name',
  title: 'This is title',
};

const HasAvatar = Template.bind({});
HasAvatar.args = {
  name: 'Name',
  hasAvatar: true,
};

export { Default, HasTitle, HasAvatar };
