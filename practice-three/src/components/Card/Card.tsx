import { memo } from 'react';

// Type
import { SIZE } from 'constants/enum';

// Component
import Avatar from 'components/Avatar/Avatar';

// Styles
import './card.scss';

interface IProps {
  hasAvatar: boolean;
  avatar: string;
  name: string;
  title: string;
  color: string;
}

const Card = (props: IProps) => {
  const { hasAvatar, avatar, name, title, color } = props;

  return (
    <div className='d-flex-center-col card'>
      {hasAvatar && <Avatar size={SIZE.LARGE} avatar={avatar} name={name} color={color} />}
      <h3 className='card__name'>{name}</h3>
      {title && <p className='card__title'>{title}</p>}
    </div>
  );
};

export default memo(Card);
