import { memo } from 'react';

// Clsx
import clsx from 'clsx';

//Interface
import { IItem } from 'types/list';

//Style
import './item.scss';

const Item = (props: IItem) => {
  const { highlight, link, value } = props;

  return (
    <li className={clsx('item', { 'item--highlight': highlight })}>
      {link ? <a href={link}>{value}</a> : value}
    </li>
  );
}

export default memo(Item);
