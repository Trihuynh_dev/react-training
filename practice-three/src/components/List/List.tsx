import { memo } from 'react';

//Font Awesome
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

//Component
import Item from './Item/Item';

//Interface
import { IListItem } from 'types/list';

//Style
import './list.scss';

library.add(fas);

const List = (props: IListItem) => {
  const { icon, headerList, count, listItem, highlight } = props;

  return (
    <ul className='list'>
      <div className='d-flex list__header'>
        <div>
          <FontAwesomeIcon icon={icon} className='list__icon' />
        </div>
        <p className='list__header__label'>
          {headerList} {count ? `(${count})` : count === 0 ? '(0)' : ''}
        </p>
      </div>
      {listItem.map((item) => (
        <Item
          key={item.id}
          value={item.value === '' ? 'Unknown' : item.value}
          highlight={highlight}
          link={item.link}
        />
      ))}
    </ul>
  );
};

export default memo(List);
