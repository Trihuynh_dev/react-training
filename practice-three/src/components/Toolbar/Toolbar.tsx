import { ChangeEvent, Dispatch, memo, SetStateAction, useState } from 'react';

// Components
import IconButton from 'components/Button/IconButton/IconButton';
import Label from 'components/Label/Label';
import SearchBar from 'components/SearchBar/SearchBar';

// Styles
import './toolbar.scss';

interface IProps {
  mode?: 'search' | 'edit';
  hasStatus?: boolean;
  statusActive?: boolean;
  hasCancel?: boolean;
  name: string;
  onClick?: () => void;
  value?: string;
  setValue?: Dispatch<SetStateAction<string>>;
  handleChangeSearch?: (event: ChangeEvent<HTMLInputElement>) => void;
  handleCloseEdit?: () => void;
}

const Toolbar = (props: IProps) => {
  const {
    name,
    statusActive,
    mode,
    hasStatus,
    hasCancel,
    onClick,
    handleCloseEdit,
    value,
    handleChangeSearch,
    setValue
  } = props;

  const [isOpened, setIsOpened] = useState(false);

  // Handle on search bar
  const handleOnSearch = () => {
    setIsOpened(true);
  };

  // Handle off search bar
  const handleOffSearch = () => {
    setIsOpened(false);

    if (setValue) {
      setValue('');
    }
  };

  // Render button cancel
  const renderButtonCancel = () => (
    <IconButton icon={['fas', 'arrow-left']} onClick={handleCloseEdit} />
  );

  // Render toobar with search bar
  const renderToolbarSearch = () => (
    <div className='dflex'>
      <IconButton icon={['fas', 'magnifying-glass']} onClick={handleOnSearch} />
    </div>
  );

  const isEdit = mode === 'edit' && !hasCancel;

  // Render toolbar
  const renderToolbar = () => (
    <div className='toolbar__title' id='title'>
      <div className='dflex'>
        {hasCancel && renderButtonCancel()}
        <h3 className='title__label'>{name}</h3>
      </div>
      {hasStatus && <Label active={statusActive!} />}
      {mode === 'search' && renderToolbarSearch()}
      {isEdit && <IconButton icon={['fas', 'pen']} onClick={onClick} />}
    </div>
  );

  // Render Search bar
  const renderSearchbar = () => (
    <SearchBar inputValue={value} onClick={handleOffSearch} onChange={handleChangeSearch} />
  );

  return <div className='toolbar'>{!isOpened ? renderToolbar() : renderSearchbar()}</div>;
};

export default memo(Toolbar);
