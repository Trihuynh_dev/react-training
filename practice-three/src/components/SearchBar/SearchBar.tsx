import React, { memo, useEffect, useRef } from 'react';

// Components
import IconButton from 'components/Button/IconButton/IconButton';

// Styles
import './searchbar.scss';

interface IProps {
  inputValue: string | undefined;
  onClick?: () => void;
  onChange?: (event: React.ChangeEvent<HTMLInputElement>) => void;
}

const SearchBar = (props: IProps) => {
  const { onClick, onChange, inputValue } = props;
  const inputRef = useRef<HTMLInputElement>(null);

  useEffect(() => {
    inputRef.current!.focus();
  }, []);

  return (
    <div className='searchbar'>
      <input
        className='searchbar__input'
        ref={inputRef}
        value={inputValue}
        onChange={onChange}
        placeholder='Search'
      />
      <IconButton icon={['fas', 'xmark']} onClick={onClick} />
    </div>
  );
};

export default memo(SearchBar);
