import { ComponentMeta, ComponentStory } from '@storybook/react';

// Enum
import { SIZE } from 'constants/enum';

// Components
import Avatar from './Avatar';

export default {
  title: 'Avatar',
  component: Avatar,
} as ComponentMeta<typeof Avatar>;

const Template: ComponentStory<typeof Avatar> = (args) => <Avatar {...args} />;

const Defaut = Template.bind({});
Defaut.args = {
  name: 'Username',
  size: SIZE.SMALL,
};

const Circle = Template.bind({});
Circle.args = {
  name: 'Username',
  size: SIZE.SMALL,
  circle: true,
};

const Medium = Template.bind({});
Medium.args = {
  name: 'Username',
  size: SIZE.MEDIUM,
};

const Large = Template.bind({});
Large.args = {
  name: 'Username',
  size: SIZE.LARGE,
};

const HasImage = Template.bind({});
HasImage.args = {
  name: 'Username',
  avatar: 'https://hinhnen123.com/wp-content/uploads/2021/07/hinh-anh-avatar-trang-11.jpg',
  size: SIZE.MEDIUM,
  circle: true,
};

export { Defaut, Circle, HasImage, Medium, Large };
