import { memo } from 'react';
import clsx from 'clsx';

// Enum
import { SIZE } from 'constants/enum';

// Styles
import './avatar.scss';

interface IProps {
  avatar?: string;
  name: string;
  circle?: boolean;
  size: SIZE.SMALL | SIZE.MEDIUM | SIZE.LARGE;
  color?: string;
}

const Avatar = (props: IProps) => {
  const { avatar, size, circle, name, color } = props;

  // Get name and Uppercase first word
  const userName = name.charAt(0).toUpperCase() || '';

  // Render avatar has image
  const renderAvatar = () => {
    return (
      <img
        alt='avatar'
        className={clsx('avatar', `avatar--${size}`, { 'avatar--border-radius': circle })}
        src={avatar}
      />
    );
  };

  // Render avatar not image
  const renderUnAvatar = () => {
    return (
      <div
        className={clsx('avatar', `avatar--${size}`, { 'avatar--border-radius': circle })}
        style={{ backgroundColor: color }}
      >
        {userName}
      </div>
    );
  };

  return <>{avatar ? renderAvatar() : renderUnAvatar()}</>;
};

export default memo(Avatar);
