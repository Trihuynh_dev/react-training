import { memo } from 'react';

// Styles
import './radioButton.scss';

interface IProps {
  label: string;
  htmlFor: string;
  checked: boolean;
  count: number;
  onChange: () => void;
}

const RadioButton = (props: IProps) => {
  const { htmlFor, label, checked, count, onChange } = props;

  return (
    <div className='radio-button' onClick={onChange} aria-hidden>
      <input
        type='radio'
        id={htmlFor}
        checked={checked}
        onChange={onChange}
        className='radio-button__input'
      />
      <label htmlFor={htmlFor} className='radio-button__label'>
        {`${label} (${count})`}
      </label>
    </div>
  );
};

export default memo(RadioButton);
