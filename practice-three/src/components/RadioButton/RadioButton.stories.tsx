import { ComponentMeta, ComponentStory } from '@storybook/react';

// Components
import RadioButton from './RadioButton';

export default {
  title: 'RadioButton',
  component: RadioButton,
} as ComponentMeta<typeof RadioButton>;

const Template: ComponentStory<typeof RadioButton> = (args) => <RadioButton {...args} />;

const NoneChecked = Template.bind({});
NoneChecked.args = {
  label: 'Radio button',
  checked: false,
  count: 0,
};

const Checked = Template.bind({});
Checked.args = {
  label: 'Radio button',
  checked: true,
  count: 0,
};

export { Checked, NoneChecked };
