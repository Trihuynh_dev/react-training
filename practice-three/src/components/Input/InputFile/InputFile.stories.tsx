import { ComponentMeta, ComponentStory } from '@storybook/react';

// Components
import InputFile from './InputFile';

export default {
  title: 'InputFile',
  component: InputFile,
} as ComponentMeta<typeof InputFile>;

const Template: ComponentStory<typeof InputFile> = (args) => <InputFile {...args} />;

const Default = Template.bind({});
Default.args = {
  label: 'Upload here',
  htmlFor: 'upload-file',
  value: '',
};

export { Default };
