import { ChangeEvent, memo } from 'react';

// Font Awesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFileUpload } from '@fortawesome/free-solid-svg-icons';

// Styles
import './inputFile.scss';

interface IProps {
  label: string;
  name: string;
  value?: string;
  htmlFor: string;
  handleChange: (e: ChangeEvent<HTMLInputElement>) => void;
}

const InputFile = (props: IProps) => {
  const { label, name, value, htmlFor, handleChange } = props;

  return (
    <div className='dflex'>
      <p className='input-box__label'>{label}:</p>
      <label htmlFor={htmlFor} className='file-upload'>
        <FontAwesomeIcon icon={faFileUpload} className='file-upload__icon' />
        {name}
      </label>
      <input id={htmlFor} type='file' onChange={handleChange} value={value} />
    </div>
  );
};

export default memo(InputFile);
