import { ComponentMeta, ComponentStory } from '@storybook/react';

// Enum
import { SIZE } from 'constants/enum';

// Components
import InputText from './InputText';

export default {
  title: 'InputText',
  component: InputText,
} as ComponentMeta<typeof InputText>;

const Template: ComponentStory<typeof InputText> = (args) => <InputText {...args} />;

const Default = Template.bind({});
Default.args = {
  label: 'label',
  name: 'Name',
  value: '',
  size: SIZE.SMALL,
};

const NoLabel = Template.bind({});
NoLabel.args = {
  value: '',
  size: SIZE.SMALL,
};

const HasPlaceholder = Template.bind({});
HasPlaceholder.args = {
  placeholder: 'Text',
};

export { Default, NoLabel, HasPlaceholder };
