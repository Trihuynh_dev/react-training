import React, { forwardRef, memo, useEffect, useState } from 'react';

// Enum
import { SIZE } from 'constants/enum';

// Styles
import './input.scss';

interface IProps {
  label?: string;
  name?: string;
  value?: string;
  placeholder?: string;
  pattern?: string;
  size: SIZE.SMALL | SIZE.LARGE;
  onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
}

const InputText = (props: IProps, ref: React.LegacyRef<HTMLInputElement>) => {
  const { label, name, value, placeholder, size, pattern } = props;
  const [valueInput, setValueInput] = useState(value);

  useEffect(() => {
    setValueInput(value);
  }, [value]);

  const handleOnChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setValueInput(event.target.value);
  };

  return (
    <div className='input-box'>
      {label && <p className='input-box__label'>{label}:</p>}
      <input
        ref={ref}
        type='text'
        className={`input-box__input input-box__input--${size}`}
        name={name}
        onChange={handleOnChange}
        value={valueInput}
        placeholder={placeholder}
        pattern={pattern}
      />
    </div>
  );
};

export default memo(forwardRef(InputText));
