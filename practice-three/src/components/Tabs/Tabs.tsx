import React, { memo } from 'react';

// Type
import { ITab } from 'types/tab';

// Components
import Tab from './Tab/Tab';

// Styles
import './tabs.scss';

interface IProps {
  list: Omit<ITab, 'index' | 'onClick'>[];
  index: string;
  onClick: (event: React.MouseEvent<HTMLButtonElement>) => void;
}

const Tabs = (props: IProps) => {
  const { list, onClick, index } = props;

  return (
    <div className='tabs'>
      {list.map((item) => (
        <Tab
          key={item.value}
          value={item.value}
          label={item.label}
          index={index}
          onClick={onClick}
        />
      ))}
    </div>
  );
};

export default memo(Tabs);
