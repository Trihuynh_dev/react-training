import React, { memo } from 'react';

// Style
import './tabPanel.scss';

interface IProps {
  index: string;
  value: string;
  children: React.ReactNode;
}

const TabPanel = (props: IProps) => {
  const { index, value, children } = props;
  const isHidden = value !== index;

  return (
    <div hidden={isHidden} className='tab__tab-panel'>
      {children}
    </div>
  );
};

export default memo(TabPanel);
