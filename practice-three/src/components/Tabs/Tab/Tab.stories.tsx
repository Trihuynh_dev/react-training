import { ComponentMeta, ComponentStory } from '@storybook/react';

import Tab from './Tab';

export default {
  title: 'Tabs/Tab',
  component: Tab,
} as ComponentMeta<typeof Tab>;

const Template: ComponentStory<typeof Tab> = (args) => <Tab {...args} />;

const Default = Template.bind({});
Default.args = {
  value: '1',
  label: 'Item one',
};

const Selected = Template.bind({});
Selected.args = {
  value: '1',
  label: 'Item one',
  index: '1',
};

export { Default, Selected };
