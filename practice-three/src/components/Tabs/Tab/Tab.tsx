import { memo } from 'react';

// Clsx
import clsx from 'clsx';

// Type
import { ITab } from 'types/tab';

// Style
import './tab.scss';

const TabOption = (props: ITab) => {
  const { value, index, label, onClick } = props;

  return (
    <button
      role='tab'
      value={value}
      className={clsx('tab', { 'tab--active': index === value })}
      onClick={onClick}
    >
      {label}
    </button>
  );
};

export default memo(TabOption);
