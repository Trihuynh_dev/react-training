import { ComponentMeta, ComponentStory } from '@storybook/react';

// Type
import { ITab } from 'types/tab';

// Components
import Tabs from './Tabs';

export default {
  title: 'Tabs',
  component: Tabs,
} as ComponentMeta<typeof Tabs>;

const list: Omit<ITab, 'index' | 'onClick'>[] = [
  {
    label: 'Tab 1',
    value: '1',
  },
  {
    label: 'Tab 2',
    value: '2',
  },
  {
    label: 'Tab 3',
    value: '3',
  },
];

const Template: ComponentStory<typeof Tabs> = (args) => <Tabs {...args} />;

const Default = Template.bind({});
Default.args = {
  list: list,
  index: '1',
  onClick: () => alert('Tab click'),
};

export { Default };
