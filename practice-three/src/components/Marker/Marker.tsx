import clsx from 'clsx';

// Font Awesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';

// Styles
import './marker.scss';

library.add(fas);

interface IProps {
  icon?: boolean;
  color?: string;
  name: string;
  disable: boolean;
}

const Marker = (props: IProps) => {
  const { icon, color, name, disable } = props;

  return (
    <div className='marker'>
      {icon && (
        <FontAwesomeIcon icon={['fas', 'user-shield']} className='marker__icon' color={color} />
      )}
      <p className={clsx('marker__name', { 'marker__name--disable': disable })}>{name}</p>
    </div>
  );
};

export default Marker;
