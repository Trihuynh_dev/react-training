import { ComponentMeta, ComponentStory } from '@storybook/react';

// Enum
import { ORDINAL_NUMBER } from 'constants/enum';

// Components
import Marker from './Marker';

export default {
  title: 'Marker',
  component: Marker,
} as ComponentMeta<typeof Marker>;

const Template: ComponentStory<typeof Marker> = (args) => <Marker {...args}/>;

const Default = Template.bind({});
Default.args = {
  icon: true,
  color: ORDINAL_NUMBER.PRIMARY,
  name: 'Marker',
};

const Disable = Template.bind({});
Disable.args = {
  name: 'Marker',
  disable: true,
};

export { Default, Disable };
