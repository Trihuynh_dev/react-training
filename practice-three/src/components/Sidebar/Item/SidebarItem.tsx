import { memo } from 'react';

// Clsx
import clsx from 'clsx';

// React Router Dom v6
import { Link, useMatch } from 'react-router-dom';

// Font Awesome
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

// Type
import { IRoutes } from 'types/routes';

// Styles
import './sidebarItem.scss';

// Library font awesome
library.add(fas);

const SidebarItem = (props: IRoutes) => {
  const { path, icon, name } = props;
  const selected = useMatch(path);

  return (
    <div>
      <li className={clsx('sidebar__item', { 'sidebar__item--selected': selected })}>
        <Link
          to={path}
          className={clsx('sidebar__item__link', { 'sidebar__item__link--selected': selected })}
        >
          <FontAwesomeIcon icon={icon} className='sidebar__item__icon' />
          <p className='sidebar__item__label'>{name}</p>
        </Link>
      </li>
    </div>
  );
};

export default memo(SidebarItem);
