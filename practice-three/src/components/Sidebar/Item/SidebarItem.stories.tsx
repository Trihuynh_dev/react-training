// Storybook
import { ComponentMeta, ComponentStory } from '@storybook/react';
import { withRouter } from 'storybook-addon-react-router-v6';

// Components
import SidebarItem from './SidebarItem';

export default {
  title: 'Sidebar/SidebarItem',
  component: SidebarItem,
  decorators: [withRouter],
} as ComponentMeta<typeof SidebarItem>;

const Template: ComponentStory<typeof SidebarItem> = (args) => <SidebarItem {...args} />;

const Default = Template.bind({});
Default.args = {
  name: 'SidebarItem',
  icon: ['fas', 'user-group'],
  path: '/a',
};

const Selected = Template.bind({});
Selected.args = {
  name: 'SidebarItem',
  icon: ['fas', 'user-group'],
  path: '/',
};

export { Default, Selected };
