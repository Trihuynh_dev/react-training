import React from 'react';

// Styles
import './sidebar.scss';

interface IProps {
  children?: React.ReactNode;
}

const Sidebar = (props: IProps) => {
  const { children } = props;

  return <div className='sidebar'>{children}</div>;
}

export default Sidebar;
