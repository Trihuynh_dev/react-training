import { IRoutes } from 'types/routes';
import SidebarItem from '../Item/SidebarItem';

// Styles
import './sidebarList.scss';

interface IProps {
  listItem: IRoutes[];
}

const SidebarList = (props: IProps) => {
  const { listItem } = props;

  return (
    <ul className='sidebar__list'>
      {listItem.map((item) => (
        <SidebarItem
          key={item.name}
          name={item.name}
          icon={item.icon}
          path={item.path}
          component={item.component}
        />
      ))}
    </ul>
  );
};

export default SidebarList;
