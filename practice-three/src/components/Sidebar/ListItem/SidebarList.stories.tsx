// Storybook
import { ComponentMeta, ComponentStory } from '@storybook/react';
import { withRouter } from 'storybook-addon-react-router-v6';

// Types
import { IRoutes } from 'types/routes';

// Components
import SidebarList from './SidebarList';

export default {
  title: 'Sidebar/SidebarList',
  component: SidebarList,
  decorators: [withRouter],
} as ComponentMeta<typeof SidebarList>;

const list: IRoutes[] = [
  {
    name: 'SidebarItem 1',
    icon: ['fas', 'user-group'],
    path: '/',
    component: () => <></>,
  },
  {
    name: 'SidebarItem 2',
    icon: ['fas', 'user-group'],
    path: '/a',
    component: () => <></>,
  },
  {
    name: 'SidebarItem 3',
    icon: ['fas', 'user-group'],
    path: '/b',
    component: () => <></>,
  },
];

const Template: ComponentStory<typeof SidebarList> = (args) => <SidebarList {...args} />;

const Default = Template.bind({});
Default.args = {
  listItem: list,
};

export { Default };
