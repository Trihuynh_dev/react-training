import clsx from 'clsx';

// Enum
import { STATUS } from 'constants/enum';

// Styles
import './label.scss';

interface IProps {
  active: boolean;
}

const Label = (props: IProps) =>{
  const { active } = props;

  return (
    <div className={clsx('label', { 'label--active': active })}>
      {active ? STATUS.ACTIVE : STATUS.NOT_ACTIVE}
    </div>
  );
};

export default Label;
