import React, { memo } from 'react';

// Styles
import './header.scss';

interface IProps {
  children?: React.ReactNode;
}

const Header = (props: IProps) => {
  const { children } = props;

  return <header className='header'>{children}</header>;
};

export default memo(Header);
