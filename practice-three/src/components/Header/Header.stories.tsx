import { ComponentMeta, ComponentStory } from '@storybook/react';

// Components
import Header from './Header';

export default {
  title: 'Header',
  component: Header,
} as ComponentMeta<typeof Header>;

const Template: ComponentStory<typeof Header> = () => <Header>Template</Header>;

const Defaut = Template.bind({});

export { Defaut };
