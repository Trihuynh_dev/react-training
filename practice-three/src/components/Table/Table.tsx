import { memo } from 'react';

// Styles
import './table.scss';

interface IProps {
  children: React.ReactNode;
}

const Table = (props: IProps) => {
  const { children } = props;

  return <table className='table'>{children}</table>;
};

export default memo(Table);
