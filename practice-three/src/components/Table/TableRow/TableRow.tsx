import React, { memo } from 'react';

// Clsx
import clsx from 'clsx';

// Styles
import './tableRow.scss';

interface IProps {
  children: React.ReactNode;
  id: number;
  index: number;
  handleClick: (event: React.FormEvent<HTMLTableRowElement>) => void;
}

const TableRow = (props: IProps) => {
  const { children, index, id, handleClick } = props;

  return (
    <tr
      id={id.toString()}
      className={clsx('table__row', { 'table__row--active': index === id })}
      onClick={handleClick}
    >
      {children}
    </tr>
  );
};

export default memo(TableRow);
