import { memo } from 'react';

// Styles
import './tableRow.scss';

const TableRowLoading = () => {
  return (
    <tr className='table__row table__row--loading'>
      <td>Loading...</td>
    </tr>
  );
};

export default memo(TableRowLoading);
