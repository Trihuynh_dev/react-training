import { memo } from 'react';

// Styles
import './tableRow.scss';

const TableRowEmpty = () => {
  return (
    <tr className='table__row table__row--loading table__row--empty'>
      <td>No rows data</td>
    </tr>
  );
};

export default memo(TableRowEmpty);
