// Storybook
import { ComponentMeta, ComponentStory } from '@storybook/react';

// Components
import Table from './Table';
import TableBody from './TableBody/TableBody';
import TableCell from './TableCell/TableCell';
import TableHead from './TableHead/TableHead';
import TableRow from './TableRow/TableRow';

export default {
  title: 'Table',
  component: Table,
} as ComponentMeta<typeof Table>;

const Template: ComponentStory<typeof Table> = () => (
  <Table>
    <TableHead listCell={['Cell Head 1', 'Cell Head 2']} />
    <TableBody>
      <TableRow id={1} index={1} handleClick={() => console.log('1')}>
        <TableCell>Cell 1</TableCell>
        <TableCell>Cell 2</TableCell>
      </TableRow>
      <TableRow id={2} index={1} handleClick={() => console.log('2')}>
        <TableCell>Cell 1</TableCell>
        <TableCell>Cell 2</TableCell>
      </TableRow>
    </TableBody>
  </Table>
);

const Default = Template.bind({});

export { Default };
