import { memo } from 'react';

// Styles
import './tableBody.scss';

interface IProps {
  children: React.ReactNode;
}

const TableBody = (props: IProps) => {
  const { children } = props;

  return <tbody className='table__body'>{children}</tbody>;
};

export default memo(TableBody);
