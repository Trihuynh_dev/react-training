import { memo } from 'react';

// Styles
import './tableHead.scss';

interface IProps {
  listCell: string[];
}

const TableHead = (props: IProps) => {
  const { listCell } = props;

  return (
    <thead className='table__head'>
      <tr className='table__row bg-light border-bot-primary'>
        {listCell.map((cell, index) => (
          <th key={index} className='table__cell'>
            {cell}
          </th>
        ))}
      </tr>
    </thead>
  );
};

export default memo(TableHead);
