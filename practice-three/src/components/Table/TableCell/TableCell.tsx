import { memo } from 'react';

// Styles
import './tableCell.scss';

interface IProps {
  children: React.ReactNode;
}

const TableCell = (props: IProps) => {
  const { children } = props;

  return <th className='table__cell'>{children}</th>;
};

export default memo(TableCell);
