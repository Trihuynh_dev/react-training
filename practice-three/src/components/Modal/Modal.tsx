import { ChangeEvent, forwardRef, LegacyRef, memo, useState } from 'react';

// Enum
import { SIZE } from 'constants/enum';

// Components
import IconButton from 'components/Button/IconButton/IconButton';
import Button from 'components/Button/NormalButton/Button';

// Styles
import './modal.scss';

interface IProps {
  stateModal: boolean;
  title: string;
  hasInput?: boolean;
  value?: string;
  btnSize?: SIZE.MEDIUM | SIZE.LARGE;
  btnNamePrimary: string;
  btnNameSecondary?: string;
  onClickBtnPrimary: () => void;
  onClickBtnSecondary?: () => void;
  handleCloseModal: () => void;
}

const Modal = (props: IProps, ref: LegacyRef<HTMLInputElement>) => {
  const [inputValue, setInputValue] = useState('');

  const {
    title,
    hasInput,
    btnNamePrimary,
    btnSize,
    btnNameSecondary,
    onClickBtnPrimary,
    onClickBtnSecondary,
    handleCloseModal,
    stateModal,
  } = props;

  const size = btnSize ? btnSize : SIZE.SMALL;

  return stateModal ? (
    <div className='modal'>
      <div className='modal__overlay'></div>
      <div className='modal__body'>
        <div className='modal__header'>
          <h3 className='modal__title'>{title}</h3>
          <IconButton
            icon={['fas', 'xmark']}
            onClick={() => {
              setInputValue('');
              handleCloseModal();
            }}
          />
        </div>
        <div className='d-flex d-flex--space-between'>
          {hasInput && (
            <input
              ref={ref}
              type='text'
              className='modal__input'
              value={inputValue}
              onChange={(event: ChangeEvent<HTMLInputElement>) => setInputValue(event.target.value)}
              pattern='/^[a-z\D]+$/i'
            />
          )}
          {btnNamePrimary && (
            <Button label={btnNamePrimary} size={size} onClick={onClickBtnPrimary} primary />
          )}
          {btnNameSecondary && (
            <Button label={btnNameSecondary} size={size} onClick={onClickBtnSecondary} />
          )}
        </div>
      </div>
    </div>
  ) : null;
};

export default memo(forwardRef(Modal));
