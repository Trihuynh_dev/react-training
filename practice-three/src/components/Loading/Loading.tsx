import { memo } from 'react';

// Font Awesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';

// Styles
import './loading.scss';

library.add(fas);

interface Iprops {
  isLoading: boolean;
  status: boolean;
}

const Loading = (props: Iprops) => {
  const { isLoading, status } = props;

  // Render Success
  const renderSuccess = () => {
    return (
      <div className='loading'>
        <FontAwesomeIcon
          icon={['fas', 'check-circle']}
          className='loading__icon loading__icon--success'
        />
        <p>Done</p>
      </div>
    );
  };

  // Render Failed
  const renderFailed = () => {
    return (
      <div className='loading'>
        <FontAwesomeIcon
          icon={['fas', 'exclamation-circle']}
          className='loading__icon loading__icon--failed'
        />
        <p>Failed</p>
      </div>
    );
  };

  return (
    <>
      {isLoading ? (
        <div className='loading'>
          <FontAwesomeIcon
            icon={['fas', 'spinner']}
            className='loading__icon loading__icon--load'
          />
          <p>Loading</p>
        </div>
      ) : status ? (
        renderSuccess()
      ) : (
        renderFailed()
      )}
    </>
  );
};

export default memo(Loading);
