import { ComponentMeta, ComponentStory } from '@storybook/react';

import Loading from './Loading';

export default {
  title: 'Loading',
  component: Loading,
} as ComponentMeta<typeof Loading>;

const Template: ComponentStory<typeof Loading> = (args) => <Loading {...args} />;

const IsLoading = Template.bind({});
IsLoading.args = {
  isLoading: true,
};

const IsDone = Template.bind({});
IsLoading.args = {
  isLoading: false,
  status: true,
};

const IsFailed = Template.bind({});
IsFailed.args = {
  isLoading: true,
  status: false,
};

export { IsLoading, IsDone, IsFailed };
