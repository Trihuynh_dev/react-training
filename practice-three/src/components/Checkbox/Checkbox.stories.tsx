import { ComponentMeta, ComponentStory } from '@storybook/react';

// Components
import Checkbox from './Checkbox';

export default {
  title: 'Checkbox',
  component: Checkbox,
} as ComponentMeta<typeof Checkbox>;

const Template: ComponentStory<typeof Checkbox> = (args) => <Checkbox {...args}>Checkbox</Checkbox>;

const NoneChecked = Template.bind({});
NoneChecked.args = {
  checked: false,
};

const Checked = Template.bind({});
Checked.args = {
  checked: true,
};

export { Checked, NoneChecked };
