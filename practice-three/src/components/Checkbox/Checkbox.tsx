import React, { memo, useEffect, useState } from 'react';

// Styles
import './checkbox.scss';

interface IProps {
  id: string;
  checked: boolean;
  children: React.ReactNode | string;
  handleCheck: (id: string, checked: boolean) => void;
}

const Checkbox = (props: IProps) => {
  const { checked, children, id, handleCheck } = props;
  const [isChecked, setIsChecked] = useState(checked);

  useEffect(() => {
    setIsChecked(checked);
  }, [checked]);

  const handleClick = (event: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
    setIsChecked(!checked);

    handleCheck(event.currentTarget.id, !checked);
  };

  const handleChecked = () => {
    setIsChecked(!checked);
  };

  return (
    <div id={id} className='checkbox' onClick={handleClick} aria-hidden={true}>
      <input
        type='checkbox'
        checked={isChecked}
        className='checkbox__input'
        onChange={handleChecked}
      />
      <label className='checkbox__label'>{children}</label>
    </div>
  );
};

export default memo(Checkbox);
