import { forwardRef, memo, useEffect, useState } from 'react';
import { ColorResult } from 'react-color';
import Twitter from 'react-color/lib/components/twitter/Twitter';

// Styles
import './colorPicker.scss';

interface IProps {
  color: string;
}

const ColorPicker = (props: IProps, ref: React.LegacyRef<HTMLDivElement>) => {
  const { color } = props;
  const [isHidden, setIsHidden] = useState(true);
  const [colorSelected, setColorSelected] = useState(color);

  useEffect(() => {
    setIsHidden(true);
    setColorSelected(color);
  }, [color]);

  useEffect(() => {
    setIsHidden(true);
  }, [colorSelected]);

  const handleChange = (colorPick: ColorResult) => {
    setColorSelected(colorPick.hex);
  };

  return (
    <div className='color-picker'>
      <p className='color-picker__label'>Color:</p>
      <div className='color-picker-box'>
        <div
          ref={ref}
          className='color-picker__area'
          style={{ backgroundColor: colorSelected }}
          onClick={() => setIsHidden(!isHidden)}
          aria-hidden='true'
        ></div>
        <div hidden={isHidden}>
          <Twitter color={color} onChange={handleChange} />
        </div>
      </div>
    </div>
  );
};

export default memo(forwardRef(ColorPicker));
