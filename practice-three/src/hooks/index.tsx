import { useContext, useEffect, useState } from 'react';

import Context from 'stores/Context';
import RoleContext from 'stores/role/RoleContext';
import RuleContext from 'stores/rule/RuleContext';
import UserContext from 'stores/user/UserContext';

// useStore
const useStore = () => {
  return useContext(Context);
};

// useUserStore
const useUserStore = () => {
  return useContext(UserContext);
};

// useUserStore
const useRoleStore = () => {
  return useContext(RoleContext);
};

// useUserStore
const useRuleStore = () => {
  return useContext(RuleContext);
};

// useDebounce
const useDebounce = (value: string, delay: number) => {
  const [debouncedValue, setDebouncedValue] = useState(value);

  useEffect(() => {
    const handler = setTimeout(() => {
      setDebouncedValue(value);
    }, delay);

    return () => {
      clearTimeout(handler);
    };
  }, [value, delay]);

  return debouncedValue;
};

export { useStore, useUserStore, useRoleStore, useRuleStore, useDebounce };
