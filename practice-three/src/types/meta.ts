interface IMeta {
  RoleRule: [number, number][];
  UserRole: [number, number][];
  UserRule: [number, number][];
}

export type { IMeta };
