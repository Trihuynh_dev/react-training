import { ReactNode } from 'react';
import { IconPrefix, IconName } from '@fortawesome/fontawesome-svg-core';

interface IRoutes {
  name: string;
  path: string;
  icon: [IconPrefix, IconName];
  component: () => JSX.Element;
  provider?: ({ children }: { children: ReactNode }) => JSX.Element;
}

export type { IRoutes };
