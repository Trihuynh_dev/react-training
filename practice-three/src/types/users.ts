enum Status {
  ACTIVE,
  NOT_ACTIVE,
}

interface IUser {
  id: number;
  name: string;
  email: string;
  avatar: string;
  status: Status.ACTIVE | Status.NOT_ACTIVE;
  roles?: number[];
  allRules?: number[];
  rules?: number[];
  RulesRoles?: { [key: string]: number[] };
}

export type { IUser };
