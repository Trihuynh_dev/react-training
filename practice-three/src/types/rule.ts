import { IRole } from './role';
import { IUser } from './users';

interface IRule {
  id: number;
  short: string;
  long: string;
  roles?: IRole[];
  users?: IUser[];
}

export type { IRule };
