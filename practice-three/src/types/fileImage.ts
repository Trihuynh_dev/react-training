interface IFileImage {
  status: string;
  value: string;
}

export type { IFileImage };
