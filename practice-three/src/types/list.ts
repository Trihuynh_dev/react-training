//Font Awesome
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas, IconName, IconPrefix } from '@fortawesome/free-solid-svg-icons';

library.add(fas);

interface IItem {
  id?: string;
  value: string;
  highlight?: boolean;
  link?: string;
}

interface IListItem {
  headerList: string;
  count?: number;
  icon: [IconPrefix, IconName];
  listItem: Omit<IItem, 'highlight'>[];
  highlight?: boolean;
}

export type { IItem, IListItem };
