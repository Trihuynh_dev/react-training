interface ITab {
  value: string;
  label: string;
  index: string;
  onClick: (event: React.MouseEvent<HTMLButtonElement>) => void;
}

export type { ITab };
