interface IRole {
  id: number;
  name: string;
  color: string;
  detail: string;
  rules?: number[];
  users?: number[];
}

export type { IRole };
