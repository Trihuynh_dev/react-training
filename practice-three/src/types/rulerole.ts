interface IRuleRole {
  id: number;
  short: string;
  long: string;
  role: string[];
}

export type { IRuleRole };
