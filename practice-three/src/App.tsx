import { Fragment } from 'react';

// React router dom
import { Route, Routes } from 'react-router-dom';

// Layout
import DefaultLayout from 'layout/DefaultLayout';

// Routes
import { publicRoutes } from 'routes';

function App() {
  return (
    <div className='App'>
      <Routes>
        {publicRoutes.map((route) => {
          const Provider = route.provider || Fragment;
          const Layout = DefaultLayout;
          const Page = route.component;

          return (
            <Route
              key={route.name}
              path={route.path}
              element={
                <Provider>
                  <Layout>
                    <Page />
                  </Layout>
                </Provider>
              }
            />
          );
        })}
      </Routes>
    </div>
  );
}

export default App;
