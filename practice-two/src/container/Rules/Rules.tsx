import { useEffect, useState } from 'react';

//Enum
import { SIZE } from '../../constants/enum';

//Interface
import { IRule } from '../../interface/rule';

//Component
import TableRule from '../../components/Table/TableRules/TableRule';
import Toolbar from '../../components/Toolbar/Toolbar';
import GridColumn from '../../layout/Grid/GridColumn/GridColumn';
import GridRow from '../../layout/Grid/GridRow/GridRow';

//Services
import { fetchRules } from '../../services/services';

const RuleManager = () =>{
  const [data, setData] = useState([] as IRule[])
  const [isLoading, setIsLoading] = useState(true)

  useEffect(() => {
    fetchDataRules()
  }, [])

  const fetchDataRules = async() => {
    const data = await fetchRules()
    if(data){
      setData(data)
      setIsLoading(false)
    }
  }

  return (
    <GridRow>
      <GridColumn size={SIZE.XLARGE}>
        <Toolbar
            mode="search"
            name="Roles"
            handleSearch={() => alert('Coming soon')}
          />
        <TableRule
          list={data}
          isLoading={isLoading}
        />
      </GridColumn>
    </GridRow>
  );
}

export default RuleManager;
