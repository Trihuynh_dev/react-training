import { useEffect, useState } from 'react';

//Hooks
import { useUserContext } from '../../hooks/hooks';

//Enum
import { SIZE } from '../../constants/enum';

//Interface
import { IUser } from '../../interface/users';

//Component
import TableUser from '../../components/Table/TableUser/TableUser';
import Toolbar from '../../components/Toolbar/Toolbar';
import FormEdit from '../../layout/Form/FormEdit/FormEdit';
import FormView from '../../layout/Form/FormView/FormView';
import GridColumn from '../../layout/Grid/GridColumn/GridColumn';
import GridRow from '../../layout/Grid/GridRow/GridRow';

//Services
import { fetchUsers } from '../../services/services';

const UserManager = () =>{
  const [dataOriginal, setDataOriginal] = useState([] as IUser[])
  const [data, setData] = useState([] as IUser[])
  const [itemActive, setItemActive] = useState({} as IUser) 
  const [isLoading, setIsLoading] = useState(true)
  const [itemId, setItemId] = useState('')
  const [editOpened, setEditOpened] = useState(false)
  const userData = useUserContext()
  useEffect(() => {
    getDataUsers()
  }, [userData?.id])

  //Get data list user
  const getDataUsers = async () => {
    const users = await fetchUsers();

    if(!users){
      setIsLoading(false);
    }
    else {
      setDataOriginal(users)
      setData(users)
      setIsLoading(false)
      setUserActive(itemId, users)
    }
  };

  //Set value itemId default
  const setDefault = () => {
    setItemId('');
    closeFormEdit();
  };

  //Handle search user
  const searchUser = async (input: string) => {
    const dataSearch = dataOriginal.filter((item: IUser) => item.name.search(input) >= 0);

    setData(dataSearch);
  };

  //Set user active
  const setUserActive = (itemId: string, data: IUser[]) => {
    const itemActive = data.find(item => item.id.toString() === itemId)

    if(itemActive) {
      setItemActive(itemActive);
    }
  };

  // Handle click row data
  const onClickRow = (event: React.MouseEvent<HTMLTableRowElement>) => {
    const id = event.currentTarget.id;
    setUserActive(id, data)
    setItemId(id)
  };

  //On Edit
  const onClickEdit = () => {
    setEditOpened(true);
  };

  //Off Edit
  const closeFormEdit = () => {
    setEditOpened(false);
  };

  const renderFormView = (data: IUser) => {
    const { name, avatar, email } = data;
    
    return (
      <FormView
        username={name}
        hasAvatar={true}
        url={avatar}
        email={{
          headerList: 'Email',
          icon: ['fas', 'envelope'],
          listItem: [{ id: '1', value: email || 'Unknown' }],
        }}
      />
    );
  };

  const renderFormEdit = (data: IUser) => {
    return (
      <FormEdit
        user={data}
        dataOnChange={getDataUsers}
        setDefault={setDefault}
      />
    );
  };

  const size = itemId === '' ? SIZE.XLARGE : SIZE.MEDIUM;

  return (
    <GridRow>
      <GridColumn size={size}>
        <Toolbar
          mode="search"
          name="Users"
          handleSearch={searchUser}
        />
        <TableUser
          list={data}
          isLoading={isLoading}
          onClickRow={onClickRow}
          itemActive={itemId}
        />
      </GridColumn>
      {size === SIZE.MEDIUM && (
        <GridColumn size={SIZE.SMALL}>
          <Toolbar
            mode="edit"
            name="User infomation"
            hasStatus={!editOpened}
            statusActive={itemActive.status ? true : false}
            onClick={onClickEdit}
            hasCancel={editOpened}
            handleCloseEdit={closeFormEdit}            
          />
          {!editOpened ? renderFormView(itemActive) : renderFormEdit(itemActive)}
        </GridColumn>
      )}
    </GridRow>
  );
}

export default UserManager;
