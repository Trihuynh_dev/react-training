import { useEffect, useState } from 'react';

//Enum
import { SIZE } from '../../constants/enum';

//Interface
import { IRole } from '../../interface/role';

//Component
import TableRole from '../../components/Table/TableRoles/TableRole';
import Toolbar from '../../components/Toolbar/Toolbar';
import GridColumn from '../../layout/Grid/GridColumn/GridColumn';
import GridRow from '../../layout/Grid/GridRow/GridRow';

//Services
import { fetchRoles } from '../../services/services';


const RoleManager = () =>{
  const [data, setData] = useState([] as IRole[])
  const [isLoading, setIsLoading] = useState(true)

  useEffect(() => {
    fetchDataRoles()
  }, [])

  const fetchDataRoles = async() => {
    const data = await fetchRoles()
    if(data){
      setData(data)
      setIsLoading(false)
    }
  }

  return (
    <GridRow>
      <GridColumn size={SIZE.XLARGE}>
        <Toolbar
            mode="search"
            name="Roles"
            handleSearch={() => alert('Coming soon')}
          />
        <TableRole
          list={data}
          isLoading={isLoading}
        />
      </GridColumn>
    </GridRow>
  );
}

export default RoleManager;
