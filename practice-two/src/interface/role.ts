interface IRole {
    id: number;
    name: string;
    detail: string;
}

export type { IRole }