//Font Awesome
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas, IconName, IconPrefix } from '@fortawesome/free-solid-svg-icons';

library.add(fas);

interface IItem {
  id?: string;
  value: string;
  highlight?: boolean;
  link?: string;
}

interface IList {
  headerList: string;
  count?: number;
  icon: [IconPrefix, IconName];
  listItem: IItem[];
}

export type { IItem, IList }