enum Status {
  ACTIVE,
  NOT_ACTIVE
}

interface IUser {
  id: number;
  name: string;
  email: string;
  avatar: string;
  status: Status.ACTIVE | Status.NOT_ACTIVE
}

export type { IUser }