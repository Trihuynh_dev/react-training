interface IRule {
    id: number;
    short: string;
    long: string;
}

export type { IRule }