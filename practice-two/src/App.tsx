import Page from "./page/Page";
import UserDataProvider from "./store/UserDataProvider";

function App() {
  return (
    <div className="App">
      <UserDataProvider>
        <Page />
      </UserDataProvider>
    </div>
  )
}

export default App;
