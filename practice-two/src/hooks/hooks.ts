import { useContext } from "react";

import UserDataContext from "../store/UserDataContext";

const useUserContext = () => {
    const userDataContext = useContext(UserDataContext)

    return userDataContext
}

export { useUserContext }