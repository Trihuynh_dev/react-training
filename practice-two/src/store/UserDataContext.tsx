import { createContext } from "react";

interface IContext {
  id: string
  setIdHandle: (id: string) => void
}

const UserDataContext = createContext<IContext | null>(null)

export default UserDataContext;