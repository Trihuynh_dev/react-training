import React, { useState } from "react";

//Context
import UserDataContext from "./UserDataContext";

interface IProps {
    children: React.ReactNode
}

const UserDataProvider = (props: IProps) =>{
  const { children } = props;
  const [id, setId] = useState('');

  const setIdHandle = (idAdd: string) => {
    setId(idAdd)
  }

  const value = {
    id,
    setIdHandle
  }

  return (
    <UserDataContext.Provider value={value}>
        {children}
    </UserDataContext.Provider>
  );
}

export default UserDataProvider;