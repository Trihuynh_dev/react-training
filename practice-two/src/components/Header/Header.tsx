import './header.scss';

import React from 'react';

interface IProps {
  children?: React.ReactNode;
}

const Header = (props:IProps) =>{
    const { children } = props;

    return (
      <header className="header">
        <h1 className="header__branch">{children}</h1>
      </header>
    );
}

export default Header;
