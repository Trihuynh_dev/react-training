import React, { useState } from 'react';
import { SIZE } from '../../constants/enum';

import IconButton from '../Button/IconButton/IconButton';
import Button from '../Button/NormalButton/Button';
import Label from '../Label/Label';
import Search from '../SearchBar/SearchBar';

import './toolbar.scss';

interface IProps {
  mode?: 'search' | 'edit';
  hasStatus?: boolean;
  statusActive?: boolean;
  hasCancel?: boolean;
  name: string;
  onClick?: () => void;
  handleSearch?: (input: string) => void;
  handleCloseEdit?: () => void;
}

const Toolbar = (props: IProps) =>{
  const {
    name,
    statusActive,
    mode,
    hasStatus,
    hasCancel,
    onClick,
    handleCloseEdit,
  } = props;

  const [isOpened, setIsOpened] = useState(false);
  const [value, setValue] = useState('');

  const handleOnSearch = () => {
    setIsOpened(true);
  };

  const handleOffSearch = () => {
    setIsOpened(false);
    setValue('')

    if(props.handleSearch){
      props.handleSearch('')
    }
  };

  const handleChangeSearch = (event: React.FormEvent<HTMLInputElement>) => {
    const input = event.currentTarget.value;
    setValue(input)

    if(props.handleSearch) {
      props.handleSearch(input)
    }
  };

  const renderButtonCancel = () => (
    <IconButton icon={['fas', 'arrow-left']} onClick={handleCloseEdit} />
  )

  const renderToolbarSearch = () => (
    <div className="dflex">
      <IconButton
        icon={['fas', 'magnifying-glass']}
        onClick={handleOnSearch}
      />
      <Button label="Role Matrix" icon={true} size={SIZE.MEDIUM} />
    </div>
  )

  const isEdit = mode === 'edit' && !hasCancel

  const renderToolbar = () => (
    <div className="toolbar__title" id="title">
      <div className="dflex">
        {hasCancel && renderButtonCancel()}
        <h3 className="title__label">{name}</h3>
      </div>
      {hasStatus && <Label active={statusActive!} />}
      {mode === 'search' && renderToolbarSearch()}
      {isEdit && (
        <IconButton icon={['fas', 'pen']} onClick={onClick} />
      )}
    </div>
  )

  const renderSearchbar = () => (
    <Search
      onClick={handleOffSearch}
      inputValue={value}
      onChange={handleChangeSearch}
    />
  )

  return (
    <div className="toolbar">
      {!isOpened ? renderToolbar() : renderSearchbar()}
    </div>
  );
}

export default Toolbar;
