import React from 'react';

import './sidebarList.scss';

interface IProps {
  children?: React.ReactNode;
}

const SidebarList = (props: IProps) => {
  const { children } = props;

  return <ul className="sidebar__list">{children}</ul>;
}

export default SidebarList;
