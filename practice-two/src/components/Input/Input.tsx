import React, { forwardRef } from 'react';
import clsx from 'clsx';

import { SIZE } from '../../constants/enum';
import './input.scss';

interface IProps {
  label?: string;
  name?: string;
  value?: string;
  placeholder?: string;
  type: 'text' | 'file' ;
  size: SIZE.SMALL | SIZE.LARGE;
  onChange?: (event: React.ChangeEvent<HTMLInputElement>) => void;
}

const Input =  forwardRef((props: IProps, ref: React.LegacyRef<HTMLInputElement>) =>{
  const {
    label,
    name,
    value, 
    placeholder,
    type,
    size,
    onChange
  } = props;

  return (
    <div className="input-box">
      {label && <p className="input-box__label">{label}:</p> }
      <input
        ref={ref}
        type={type}
        className={clsx('input-box__input', `input-box__input--${size}`)}
        name={name}
        onChange={onChange}
        value={value}
        placeholder={placeholder}
      />
    </div>
  );
})

export default Input;
