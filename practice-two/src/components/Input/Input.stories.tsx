import { ComponentMeta, ComponentStory } from '@storybook/react';
import { SIZE } from '../../constants/enum';

import Input from './Input';

export default {
  title: 'Input',
  component: Input,
} as ComponentMeta<typeof Input>;

const Template: ComponentStory<typeof Input> = (args) => <Input {...args} />;

const Default = Template.bind({});
Default.args = {
  type: 'text',
  label: 'label',
  name: 'Name',
  value: '',
  size: SIZE.SMALL
};

const NoLabel = Template.bind({});
NoLabel.args = {
  name: 'Name',
  value: '',
  size: SIZE.SMALL
};

const File = Template.bind({});
File.args = {
  type: 'file',
  size: SIZE.SMALL
};

const HasPlaceholder = Template.bind({})
HasPlaceholder.args = {
  type: 'text',
  placeholder: 'Text'
}

export { Default, NoLabel, File, HasPlaceholder };
