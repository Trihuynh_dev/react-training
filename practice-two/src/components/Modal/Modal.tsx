import React, { forwardRef, LegacyRef } from 'react';
import { SIZE } from '../../constants/enum';

import IconButton from '../Button/IconButton/IconButton';
import Button from '../Button/NormalButton/Button';

import './modal.scss';

interface IProps {
  stateModal: boolean;
  title: string;
  hasInput?: boolean;
  value: string;
  btnNamePrimary: string;
  btnNameSecondary?: string;
  onClickBtnPrimary: () => void;
  onClickBtnSecondary?: () => void;
  handleCloseModal: () => void;
  onChange: (event: React.FormEvent<HTMLInputElement>) => void;
}

const Modal = (props: IProps, ref: LegacyRef<HTMLInputElement>) =>{
  const {
    title,
    hasInput,
    value,
    btnNamePrimary,
    btnNameSecondary,
    onClickBtnPrimary,
    onClickBtnSecondary,
    handleCloseModal,
    onChange,
    stateModal
  } = props;

  return (
    stateModal ? (
      <div className="modal">
        <div className="modal__overlay"></div>
        <div className="modal__body">
          <div className="modal__header">
            <h3 className="modal__title">{title}</h3>
            <IconButton icon={['fas', 'xmark']} onClick={handleCloseModal} />
          </div>
          <div className="d-flex d-flex--space-between">
            {hasInput && (
              <input
                ref={ref}
                type="text"
                className="modal__input"
                value={value}
                onChange={onChange}
              />
            )}
            {btnNamePrimary && (
              <Button
                label={btnNamePrimary}
                size={SIZE.SMALL}
                onClick={onClickBtnPrimary}
              />
            )}
            {btnNameSecondary && (
              <Button
                label={btnNameSecondary}
                size={SIZE.SMALL}
                onClick={onClickBtnSecondary}
              />
            )}
          </div>
        </div>
      </div>
    ) : null
  )
}

export default forwardRef(Modal);
