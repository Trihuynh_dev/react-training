import React from 'react';
import clsx from 'clsx';

import './tabOption.scss'

interface IProps {
  id: string;
  label: string;
  index?: string;
  onClick: (event: React.MouseEvent<HTMLButtonElement>) => void;
}

const TabOption = (props:IProps) =>{
  const { id, index, label, onClick } = props;

  return (
    <button
      role="tab"
      id={id}
      className={clsx('tab__tab-option', { 'tab__tab-option--active': index === id })}
      onClick={onClick}
    >
      {label}
    </button>
  );
}

export default TabOption;
