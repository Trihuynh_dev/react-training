import { ComponentMeta, ComponentStory } from '@storybook/react';

import TabOption from './TabOption';

export default {
  title: 'TabOption',
  component: TabOption,
} as ComponentMeta<typeof TabOption>;

const Template: ComponentStory<typeof TabOption> = (args) => <TabOption {...args} />;

const Default = Template.bind({});
Default.args = {
  id: '1',
  label: 'Item one',
};

const Active = Template.bind({});
Active.args = {
  id: '1',
  label: 'Item one',
  index: '1'
};

export { Default, Active };
