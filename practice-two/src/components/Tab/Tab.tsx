import React, { useState } from 'react';

import TabOption from './TabOption/TabOption';
import TabPanel from './TabPanel/TabPanel';

import './tab.scss';

const Tab = () => {
  const [index, setIndex] = useState('1');

  const handleChange = (event: React.MouseEvent<HTMLButtonElement>) => {
    setIndex(event.currentTarget.id);
  };

  return (
    <div className="tab">
      <div className="tab__options">
        <TabOption
          label="Tab Option 1"
          id="1"
          index={index}
          onClick={handleChange}
        />
        <TabOption
          label="Tab Option 2"
          id="2"
          index={index}
          onClick={handleChange}
        />
        <TabOption
          label="Tab Option 3"
          id="3"
          index={index}
          onClick={handleChange}
        />
      </div>
      <div className="tab__panels">
        <TabPanel index={index} id="1">
          <h3>Tab Panel 1</h3>
        </TabPanel>
        <TabPanel index={index} id="2">
          <h3>Tab Panel 2</h3>
        </TabPanel>
        <TabPanel index={index} id="3">
          <h3>Tab Panel 3</h3>
        </TabPanel>
      </div>
    </div>
  );
}

export default Tab;
