import { ComponentMeta, ComponentStory } from '@storybook/react';

import Tab from './Tab';

export default {
  title: 'Tab',
  component: Tab,
} as ComponentMeta<typeof Tab>;

const Template: ComponentStory<typeof Tab> = () => <Tab />;

const Default = Template.bind({});

export { Default };