import React from 'react';
import clsx from 'clsx';

import './tabPanel.scss';

interface IProps {
  index: string,
  id: string,
  children: React.ReactNode
}

const TabPanel = (props: IProps) =>{
  const { index, id, children } = props;

  return (
    <div 
      className={clsx("tab__tab-panel", {"tab__tab-panel--active": index === id})}
      id={id}
    >
      {children}
    </div>
  );
}
  
export default TabPanel;