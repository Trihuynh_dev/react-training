import './button.scss';

import { faTableList } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import clsx from 'clsx';
import { SIZE } from '../../../constants/enum';

interface IProps {
  primary?: boolean;
  size: SIZE.SMALL | SIZE.MEDIUM | SIZE.LARGE;
  label: string;
  icon?: true;
  onClick?: () => void;
}

const Button = (props: IProps) =>{
  const { primary, size, label, icon, onClick } = props;

  return (
    <button
      type="button"
      className={clsx('btn', `btn--${size}`, { 'btn--primary': primary })}
      onClick={onClick}
    >
      {icon && <FontAwesomeIcon icon={faTableList} className="mg-right-s" />}
      {label}
    </button>
  );
}

export default Button;
