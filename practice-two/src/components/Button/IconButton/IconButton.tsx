import './iconButton.scss';

import { library } from '@fortawesome/fontawesome-svg-core';
import { fas, IconName, IconPrefix } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

library.add(fas);

interface IProps {
  icon: [IconPrefix, IconName];
  onClick?: () => void;
}

const IconButton = (props: IProps) =>{
  const { icon, onClick } = props;

  return (
    <div className="box-icon" onClick={onClick}>
      <FontAwesomeIcon icon={icon} className="icon" />
    </div>
  );
}

export default IconButton;
