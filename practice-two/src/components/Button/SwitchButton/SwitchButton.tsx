import './switchButton.scss';

import React from 'react';

interface IProps {
  checked: boolean;
  onChange?: (event: React.ChangeEvent<HTMLInputElement>) => void;
}

const SwitchButton = (props: IProps) => {
  const { checked, onChange } = props;

  return (
    <label className="switch">
      <input
        type="checkbox" 
        checked={checked} 
        onChange={onChange}
      />
      <span className="slider round"></span>
    </label>
  );
}

export default SwitchButton;
