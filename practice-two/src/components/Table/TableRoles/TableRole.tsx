import { memo } from 'react';

//Interface
import { IRole } from '../../../interface/role';

//Component
import TableRowEmpty from '../TableRow/TableRowEmpty';
import TableRowLoading from '../TableRow/TableRowLoading';
import TableRoleRow from './TableRoleRow';

//Style
import '../table.scss';

interface IProps {
  list: IRole[];
  isLoading: boolean;
}

const TableRole = (props: IProps) =>{
  const renderRow = (listRow: IProps) => {
    const { isLoading, list } = listRow;
    
    if (isLoading) {
      return <TableRowLoading />;
    } 
    
    if (list.length === 0) {
      return <TableRowEmpty />;
    }

    return list.map((role) => <TableRoleRow key={role.id} {...role} />);
  };

  return (
    <table className="table">
      <thead className="table__head">
        <tr className="table__row bg-light border-bot-primary">
          <th className="table__col"></th>
          <th className="table__col">Name</th>
        </tr>
      </thead>
      <tbody className="table__body">{renderRow(props)}</tbody>
    </table>
  );
}

export default memo(TableRole);
