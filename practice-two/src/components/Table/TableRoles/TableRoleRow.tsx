import { memo } from 'react';

//Interface
import { IRole } from '../../../interface/role';

//Component
import Avatar from '../../Avatar/Avatar';

//Style
import '../table.scss';

const TableRoleRow = (props: IRole) =>{
  const { id, name } = props;

  return (
    <tr className="table__row" id={id.toString()}>
      <td className="table__col">
        <Avatar 
          username={name}
          circle={true} 
          size="small" 
        />
      </td>
      <td className="table__col">{name}</td>
    </tr>
  );
}

export default memo(TableRoleRow);
