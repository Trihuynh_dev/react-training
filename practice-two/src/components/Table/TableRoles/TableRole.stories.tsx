import { ComponentMeta, ComponentStory } from '@storybook/react';

//Interface
import { IRole } from '../../../interface/role';

//Component
import TableRole from './TableRole';

export default {
  title: 'Table/TableRole',
  component: TableRole,
} as ComponentMeta<typeof TableRole>;

const dataRole: IRole[] = [
  {
    id: 1,
    name: 'Role 1',
    detail: ''
  },
  {
    id: 2,
    name: 'Role 2',
    detail: ''
  },
];
const emptyDataUser: IRole[] = [];

const Template: ComponentStory<typeof TableRole> = (args) => <TableRole {...args} />;

const Default = Template.bind({});
Default.args = {
  list: dataRole,
};

const Empty = Template.bind({});
Empty.args = {
  list: emptyDataUser,
};

const Loading = Template.bind({});
Loading.args = {
  list: dataRole,
  isLoading: true,
};

export { Default, Empty, Loading };
