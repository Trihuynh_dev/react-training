import { memo } from 'react';

//Interface
import { IRule } from '../../../interface/rule';

//Component
import TableRowEmpty from '../TableRow/TableRowEmpty';
import TableRowLoading from '../TableRow/TableRowLoading';
import TableRuleRow from './TableRuleRow';

//Style
import '../table.scss';

interface IProps {
  list: IRule[];
  isLoading: boolean;
}

const TableRule = (props: IProps) =>{
  const renderRow = (listRow: IProps) => {
    const { isLoading, list } = listRow;

    if (isLoading) {
      return <TableRowLoading />;
    }
    
    if (list.length === 0) {
      return <TableRowEmpty />;
    }

    return list.map((rule) => <TableRuleRow key={rule.id} {...rule} />);
  };

  return (
    <table className="table">
      <thead className="table__head">
        <tr className="table__row bg-light border-bot-primary">
          <th className="table__col">Name</th>
          <th className="table__col">Description</th>
        </tr>
      </thead>
      <tbody className="table__body">{renderRow(props)}</tbody>
    </table>
  );
}

export default memo(TableRule);
