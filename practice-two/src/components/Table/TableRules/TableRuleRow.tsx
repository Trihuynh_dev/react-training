import { memo } from 'react';

//Interface
import { IRule } from '../../../interface/rule';

import '../table.scss';

const TableRoleRow = (props: IRule) =>{
  const { id, short, long } = props;

  return (
    <tr className="table__row" id={id.toString()}>
      <td className="table__col">{short}</td>
      <td className="table__col">{long}</td>
    </tr>
  );
}

export default memo(TableRoleRow);
