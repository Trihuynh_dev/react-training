import './tableRow.scss';

const TableRowEmpty  = () => {
  return (
    <tr className='table__row table__row--loading table__row--empty'>
      <td>Empty!!! You can add new</td>
    </tr>
  );
}

export default TableRowEmpty;
