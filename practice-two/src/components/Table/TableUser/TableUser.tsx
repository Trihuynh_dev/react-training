import React, { memo } from 'react';

//Inteface
import { IUser } from '../../../interface/users';

//Component
import TableRowEmpty from '../TableRow/TableRowEmpty';
import TableRowLoading from '../TableRow/TableRowLoading';
import TableUserRow from './TableUserRow';


interface IProps {
  list: IUser[];
  isLoading: boolean;
  onClickRow: (event: React.MouseEvent<HTMLTableRowElement>) => void;
  itemActive: string;
}

const TableUser  = (props: IProps) =>{
  const renderRow = ( listRow: IProps) => {
    const { isLoading, list, itemActive, onClickRow } = listRow;

    if (isLoading) {
      return <TableRowLoading />;
    }

    if (list.length === 0) {
      return <TableRowEmpty />;
    } 

    return list.map((item) => (
          <TableUserRow
            key={item.id}
            user={item}
            onClickRow={onClickRow}
            itemActive={itemActive}
          />
    ));
  };

  return (
    <table className="table">
      <thead className="table__head">
        <tr className="table__row bg-light border-bot-primary">
          <th className="table__col"></th>
          <th className="table__col">Full name</th>
          <th className="table__col">Status</th>
          <th className="table__col">Email</th>
        </tr>
      </thead>
      <tbody className="table__body">
        {renderRow(props)}
      </tbody>
    </table>
  );
}

export default memo(TableUser);
