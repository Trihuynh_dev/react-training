import { memo } from 'react';
import clsx from 'clsx';

//Interface
import { IUser } from '../../../interface/users';

//Component
import Avatar from '../../Avatar/Avatar';
import Label from '../../Label/Label';

//Style
import '../table.scss';

interface IProps {
  user: IUser
  itemActive?: string;
  onClickRow?: (event: React.MouseEvent<HTMLTableRowElement>) => void;
}

const TableUserRow = (props: IProps) => {
  const { itemActive, onClickRow } = props;
  const { id, name, status, email, avatar } = props.user;

  return (
    <tr
      className={clsx('table__row',
        {'table__row--active': itemActive === id.toString()}
      )}
      id={id.toString()}
      onClick={onClickRow}
    >
      <td className="table__col">
        <Avatar
          url={avatar}
          username={name}
          circle={true}
          size="small"
        />
      </td>
      <td className="table__col">{name}</td>
      <td className="table__col">
        <Label active={Boolean(status)} />
      </td>
      <td className="table__col">{email}</td>
    </tr>
  );
}

export default memo(TableUserRow);