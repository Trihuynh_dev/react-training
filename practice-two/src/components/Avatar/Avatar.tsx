import clsx from 'clsx';

import './avatar.scss';

interface IProps {
  url?: string;
  username: string;
  circle?: boolean;
  size?: 'small' | 'medium' | 'large';
}

const Avatar = (props: IProps) => {
  const { url, size, circle, username } = props;
  let userName = username ? username.charAt(0).toUpperCase() : '';

  const renderImg = () => {
    return (
      <img
        className={clsx('avatar', `avatar--${size}`, { 'avatar--border-radius': circle })}
        src={url}
      />
    )
  }

  const renderBlock = () => {
    return (
      <div
        className={clsx('avatar', `avatar--${size}`, { 'avatar--border-radius': circle })}
      >
        {userName}
      </div>
    )
  }

  return (
    <>
      {url ? renderImg() : renderBlock()}
    </>
  )
}

export default Avatar;
