import React, { useEffect, useRef } from 'react';

import IconButton from '../Button/IconButton/IconButton';
import Input from '../Input/Input';
import { SIZE } from '../../constants/enum';
import './searchbar.scss';

interface IProps {
  inputValue: string | undefined;
  onClick?: () => void;
  onChange?: (event: React.FormEvent<HTMLInputElement>) => void;
}

const SearchBar = (props: IProps) =>{
  const { onClick, onChange, inputValue } = props;
  const inputRef = useRef<HTMLInputElement>(null);

  useEffect(()=>{
    inputRef.current!.focus();
  }, [])

  return(
    <div className="searchbar" id="search">
        <Input
          ref={inputRef}
          type='text'
          size={SIZE.LARGE}
          value={inputValue}
          onChange={onChange}
          placeholder='Search'
        />
        <IconButton icon={['fas', 'xmark']} onClick={onClick} />
    </div>
  );
}

export default SearchBar;
