import { memo } from 'react';

//Font Awesome
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas, IconName, IconPrefix } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

//Component
import Item from './Item/Item';

//Interface
import { IList } from '../../interface/list';

//Style
import './list.scss';

library.add(fas);

const List = (props: IList) =>{
  const { 
    icon,
    headerList,
    count,
    listItem
  } = props;

  return (
    <ul className="list">
      <div className="d-flex list__header">
        <div>
          <FontAwesomeIcon icon={icon} />
        </div>
        <p className="list__header__label">
          {headerList} {count && `(${count})`}
        </p>
      </div>
      {listItem.map((item) => (
        <Item
          key={item.id}
          value={item.value}
          highlight={item.highlight}
          link={item.link}
        />
      ))}
    </ul>
  );
}

export default memo(List);
