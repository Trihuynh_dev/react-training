import { ComponentMeta, ComponentStory } from '@storybook/react';

import { IItem } from '../../interface/list';
import List from './List';

export default {
  title: 'List',
  component: List,
} as ComponentMeta<typeof List>;

const list: IItem[] = [
  {
    id: '1',
    value: 'Item 1',
    highlight: false,
  },
  {
    id: '2',
    value: 'Item 2',
    highlight: true,
  },
  {
    id: '3',
    value: 'Item 3',
    highlight: true,
  }
];

const Template: ComponentStory<typeof List> = (args) => <List {...args} />;

const Default = Template.bind({});
Default.args = {
  icon: ['fas', 'envelope'],
  headerList: 'List',
  listItem: list,
};

export { Default };
