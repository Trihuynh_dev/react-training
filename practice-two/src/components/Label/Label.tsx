import './label.scss';

import clsx from 'clsx';
import { STATUS } from '../../constants/enum';

interface IProps {
  active: boolean;
}

const Label = (props: IProps) =>{
  const { active } = props;

  return (
    <div className={clsx('label', { 'label--active': active })}>
      {active ? STATUS.ACTIVE : STATUS.NOT_ACTIVE}
    </div>
  );
}

export default Label;
