import axios from 'axios';

import { BASE_URL_API, STATUS_CODE } from '../constants/api';
import { IFileImage } from '../interface/fileImage';

//Interface
import { IRole } from '../interface/role';
import { IRule } from '../interface/rule';
import { IUser } from '../interface/users';

axios.defaults.baseURL = BASE_URL_API;

/**
 * @des Fetch data from base URL
 * @returns {Promise}
 */
const fetchUsers = async (): Promise<IUser[] | null> => {
  const res = await axios.get(`/users`);

  if (res.status === STATUS_CODE.OK) {
    return res.data;
  }

  return null;
};

const fetchRoles = async (): Promise<IRole[] | null> => {
  const res = await axios.get(`/roles`);

  if (res.status === STATUS_CODE.OK) {
    return res.data;
  }

  return null;
};

const fetchRules = async (): Promise<IRule[] | null> => {
  const res = await axios.get(`/rules`);

  if (res.status === STATUS_CODE.OK) {
    return res.data;
  }

  return null;
};

/**
 * @param {object} payload
 * @returns {Promise}
 */
const createUser = async (payload: Omit<IUser, 'id'>): Promise<{id: number} | null> => {
  const res = await axios.post('/users', payload);

  if (res.status === STATUS_CODE.OK) {
    return res.data;
  }

  return null;
};

/**
 * @param {number} id
 * @returns {Promise}
 */
const removeUser = async (id: number): Promise<{id: number} | null> => {
  const res = await axios.delete(`/users/${id}`);

  if (res.status === STATUS_CODE.OK) {
    return res.data;
  }

  return null;
};

/**
 * @param {(string | number)} id
 * @param {Object} payload
 * @returns {Promise}
 */
const updateUser = async ( id: string | number, payload: object ): Promise<{id: number} | null> => {
  const res = await axios.put(`/users/${id}`, payload);

  if (res.status === STATUS_CODE.OK) {
    return res.data;
  }

  return null;
};

/**
 * @param {(string | number)} id
 * @param {FormData} payload {upload: file, upload_fullpath: file.name}
 * @returns {Promise}
 */
const uploadAvatar = async ( id: string | number, payload: object ): Promise<IFileImage | null> => {
  const res = await axios.post(`/users/${id}/avatar`, payload);

  if (res.status === STATUS_CODE.OK) {
    return res.data;
  }

  return null;
};

export { createUser, fetchUsers, fetchRoles, fetchRules, removeUser, updateUser, uploadAvatar };
