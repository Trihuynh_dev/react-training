//Component
import RoleManager from '../../container/Roles/Roles';
import RuleManager from '../../container/Rules/Rules';
import UserManager from '../../container/User/UserManager';

//Style
import './layoutMain.scss';

interface IProps {
  index: string
}

const LayoutMain = (props: IProps) =>{
  const { index } = props;

  return (
    <div className="layout-main">
      {index === 'sidebar-item-one' && <UserManager />}
      {index === 'sidebar-item-two' && <RoleManager />}
      {index === 'sidebar-item-three' && <RuleManager />}
    </div>
  );
}

export default LayoutMain;
