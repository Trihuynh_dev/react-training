import React from 'react';
import clsx from 'clsx';

import { SIZE } from '../../../constants/enum';
import './gridColumn.scss';

interface IProps {
  size: 
      SIZE.XSMALL
    | SIZE.SMALL
    | SIZE.MEDIUM
    | SIZE.LARGE
    | SIZE.XLARGE;
  children?: React.ReactNode | React.ReactNode[];
}

const GridColumn = (props: IProps) =>{
  const { size, children } = props;

  return (
    <div className={clsx('grid__column', `grid__column--${size}`)}>
      {children}
    </div>
  );
}

export default GridColumn;
