import { memo, useEffect, useState } from 'react';

//Interface
import { IList } from '../../../interface/list';

//Component
import Avatar from '../../../components/Avatar/Avatar';
import List from '../../../components/List/List';

//Enum
import { SIZE } from '../../../constants/enum';

//Style
import './formView.scss';


interface IProps {
  username: string;
  url?: string;
  hasAvatar?: boolean;
  title?: string;
  email?: IList;
  lastVisited?: IList;
  users?: IList;
  roles?: IList;
  rules?: IList;
}

const FormView = (props: IProps) =>{
  const { 
    rules,
    roles,
    users,
    hasAvatar,
    username,
    title,
    email,
    lastVisited,
    url 
  } = props;

  return (
    <div>
      <div className="d-flex-center-col padding-l">
        {hasAvatar && 
          <Avatar 
            size={SIZE.LARGE}
            url={url}
            username={username} 
          />}
        <h3>{username}</h3>
        {title && <p>{title}</p>}
      </div>
      {email && <List {...email} />}
      {lastVisited && <List {...lastVisited} />}
      {rules && <List {...rules} />}
      {roles && <List {...roles} />}
      {users && <List {...users} />}
    </div>
  );
}

export default memo(FormView);
