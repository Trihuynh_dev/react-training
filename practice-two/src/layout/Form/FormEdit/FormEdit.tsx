import React, { memo, useEffect, useState } from 'react';

//Hooks
import { useUserContext } from '../../../hooks/hooks';

//Component
import Avatar from '../../../components/Avatar/Avatar';
import Button from '../../../components/Button/NormalButton/Button';
import SwitchButton from '../../../components/Button/SwitchButton/SwitchButton';
import Input from '../../../components/Input/Input';
import Label from '../../../components/Label/Label';

//Interface
import { IUser } from '../../../interface/users';

//Enum
import { SIZE } from '../../../constants/enum';

//Services
import { removeUser, updateUser, uploadAvatar } from '../../../services/services';

//Style
import './formEdit.scss';

interface IProps {
  user: IUser;
  dataOnChange: () => void;
  setDefault: () => void;
}

const FormEdit = (props: IProps) => {
  const { user, dataOnChange, setDefault } = props;
  const [dataUser, setDataUser] = useState(user)
  const userDataContext = useUserContext()
  let status = Boolean(dataUser.status)

  useEffect( () => {
    setDataUser(user)
  }, [user.id] )

  //Delete user
  const handleDelete = async () => {
    const idRemove = await removeUser(dataUser.id)
    userDataContext?.setIdHandle(idRemove!.id.toString())
    setDefault();
    dataOnChange();
  };

  //Save edit user
  const handleSave = async () => {
    await updateUser(dataUser.id, dataUser)
    dataOnChange();
  };

  const handleChangeInput = (event: React.ChangeEvent<HTMLInputElement>) => {
    const name = event.target.name;

    setDataUser({ ...dataUser, [name]: event.target.value})
  };

  const handleChangeCheckbox = (event: React.ChangeEvent<HTMLInputElement>) => {
    setDataUser({ ...dataUser, status: +event.target.checked});
  };

  const handleChangeFile = async (event: React.ChangeEvent<HTMLInputElement>) => {
    const file = event.target.files![0];

    const formData = new FormData();
    formData.append('upload', file);
    formData.append('upload_fullpath', file.name);

    const data = await uploadAvatar(dataUser.id, formData);
    setDataUser({...dataUser, avatar: data!.value || ''})
  };

  return (
    <div className="padding-m">
      <div className="align-left">
        <Button
          size={SIZE.SMALL}
          label="Delete"
          onClick={handleDelete}
        />
        <Button
          size={SIZE.SMALL}
          label="Save"
          primary={true}
          onClick={handleSave}
        />
      </div>
      <div className="mg-top-m">
        <Input
          size={SIZE.SMALL}
          label="Full name"
          name="name"
          value={dataUser.name}
          type="text"
          onChange={handleChangeInput}
        />
        <Input
          size={SIZE.SMALL}
          label="Email"
          name="email"
          value={dataUser.email}
          type="text"
          onChange={handleChangeInput}
        />
        <Input 
          label="Avatar" 
          type="file" 
          onChange={handleChangeFile} 
          size={SIZE.SMALL}
        />
        <Avatar 
          size={SIZE.MEDIUM} 
          url={dataUser.avatar} 
          username={dataUser.name}
        />
        <div className='input-box justify-start'>
          <p className="input-box__label">Status:</p>
          <SwitchButton checked={status} onChange={handleChangeCheckbox} />
          <Label active={status} />
        </div>
      </div>
    </div>
  );
}

export default memo(FormEdit);
