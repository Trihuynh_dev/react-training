import React, { Fragment, useState, useContext, useRef, useEffect } from 'react';

import UserDataContext from '../store/UserDataContext';

//Component
import Button from '../components/Button/NormalButton/Button';
import Modal from '../components/Modal/Modal';
import SidebarItem from '../components/Sidebar/Item/SidebarItem';
import SidebarList from '../components/Sidebar/ListItem/SidebarList';
import Sidebar from '../components/Sidebar/Sidebar';
import Grid from '../layout/Grid/Grid';
import GridColumn from '../layout/Grid/GridColumn/GridColumn';
import GridRow from '../layout/Grid/GridRow/GridRow';
import Header from '../components/Header/Header';
import LayoutMain from '../layout/Main/LayoutMain';

//Enum
import { SIZE } from '../constants/enum';

//Services
import { createUser } from '../services/services';

interface IProps {
  children?: React.ReactNode;
}

const Page = (props: IProps) =>{
  const [index, setIndex] = useState('sidebar-item-one')
  const [inputValue, setInputValue] = useState('')
  const [stateModal, setStateModal] = useState(false)

  const UserData = useContext(UserDataContext)

  const handleClick = (event: React.MouseEvent<HTMLLIElement>) => {
    setIndex(event.currentTarget.id);
  };

  const handleChangeText = (event: React.FormEvent<HTMLInputElement>) => {
    setInputValue(event.currentTarget.value);
  };

  const openModal = () => {
    setStateModal(true);
  };

  const closeModal = () => {
    setStateModal(false);
  };

  const addUser = async (username: string) => {
    const payload = {
      name: username,
      email: '',
      status: 0,
      avatar: '',
    };

    const idAdd = await createUser(payload);

    UserData?.setIdHandle(idAdd?.id+'')

    closeModal();
  };

  return (
    <Fragment>
      <Header>Users Manager</Header>
      <Grid>
        <GridRow>
          <GridColumn size={SIZE.XSMALL}>
            <Sidebar>
              <Button
                label="+ New"
                primary={true}
                size={SIZE.LARGE}
                onClick={openModal}
              />
              <SidebarList>
                <SidebarItem
                  id="sidebar-item-one"
                  index={index}
                  label="Users"
                  icon={['fas', 'user-group']}
                  onClick={handleClick}
                />
                <SidebarItem
                  id="sidebar-item-two"
                  index={index}
                  label="Roles"
                  icon={['fas', 'clipboard-check']}
                  onClick={handleClick}
                />
                <SidebarItem
                  id="sidebar-item-three"
                  index={index}
                  label="Rules"
                  icon={['fas', 'user-check']}
                  onClick={handleClick}
                />
              </SidebarList>
            </Sidebar>
          </GridColumn>
          <GridColumn size={SIZE.LARGE}>
            <LayoutMain index={index} />
          </GridColumn>
        </GridRow>
      </Grid>
      <Modal
        stateModal={stateModal}
        title="Enter username"
        btnNamePrimary="Save"
        onClickBtnPrimary={() => addUser(inputValue)}
        value={inputValue}
        onChange={handleChangeText}
        handleCloseModal={closeModal}
        hasInput={true}
      />
    </Fragment>
  );
}

export default Page;
