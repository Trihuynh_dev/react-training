import './sidebarList.scss';

import React from 'react';

interface IProps {
  children?: React.ReactNode;
}

class SidebarList extends React.Component<IProps> {
  render() {
    const { children } = this.props;

    return <ul className="sidebar__list">{children}</ul>;
  }
}

export default SidebarList;
