# React Training
## Practice One
#### Timeline: 25/07/2022 - 05/08/2022
### Author: Tri Huynh
### Technical
- React
- TypeScript
- Sass
- Storybook
### How to run the project
##### Step one: **Clone the code folder from github to your device**
- Choose a path to save that file -> At that path open the command window
- Run command
  > `git clone https://github.com/HVT11/react-training.git`
##### Step two: **Run project**
- Change the path to the folder you just cloned to your computer
  > `cd react-training`
- Checkout branch feature/practice-one
  > `git checkout feature/practice-one`
- Change the path to the folder practice-one
  > `cd practice-one`
- Install npm
  > `npm install`
- Run storybook
  > `npm run storybook`
- Run start project
  > `npm start`
- Hold `ctrl` and `click` on the successfully created localhost link to view the website
