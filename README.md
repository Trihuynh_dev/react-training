# React Training
### Author: Tri Huynh
## Practice One
#### Timeline: 25/07/2022 - 05/08/2022
### Technical
- React
- TypeScript
- Sass
- Storybook
### How to run the project
##### Step one: **Clone the code folder from github to your device**
- Choose a path to save that file -> At that path open the command window
- Run command
  > `git clone https://github.com/HVT11/react-training.git`
##### Step two: **Run project**
- Change the path to the folder you just cloned to your computer
  > `cd react-training`
- Checkout branch develop
  > `git checkout develop`
- Change the path to the folder practice-one
  > `cd practice-one`
- Install npm
  > `npm install`
- Run storybook
  > `npm run storybook`  
  View on port: `http://localhost:6006`
- Run start project
  > `npm start`
- Hold `ctrl` and `click` on port: `http://127.0.0.1:5173`
## Practice Two
#### Timeline: 19/08/2022 - 27/08/2022
### Technical
- React Hook
- TypeScript
- Sass
- Storybook
### How to run the project
##### Step one: **Clone the code folder from github to your device**
- Choose a path to save that file -> At that path open the command window
- Run command
  > `git clone https://gitlab.com/Trihuynh_dev/react-training.git`
##### Step two: **Run project**
- Change the path to the folder you just cloned to your computer
  > `cd react-training`
- Checkout branch develop
  > `git checkout develop`
- Change the path to the folder practice-one
  > `cd practice-two`
- Install npm
  > `npm install`
- Run storybook
  > `npm run storybook`  
  View on port: `http://localhost:6006`
- Run start project
  > `npm start`
- Hold `ctrl` and `click` on port: `http://127.0.0.1:5173`
## Practice Three
#### Timeline: 05/09/2022 - 27/09/2022
#### Development Plan: 
- [Document](https://docs.google.com/document/d/1h5a7L2Crdc7Kieb7kFk87D8_A2io4RY2QLqq2-vK5_s/edit?usp=sharing)
- [Board](https://gitlab.com/Trihuynh_dev/react-training/-/boards/4718306)
### Technical
- React Hook
- React Router Dom
- TypeScript
- Sass
- Storybook
### View web deloy
- [Link](https://trihuynh-react-practice-three.herokuapp.com/)
### Workflow
- ![ALT](./practice-three/src/assets/images/flow-chart.png)
### How to run the project
##### Step one: **Clone the code folder from github to your device**
- Choose a path to save that file -> At that path open the command window
- Run command
```
git clone https://gitlab.com/Trihuynh_dev/react-training.git
```
##### Step two: **Run project**
- Change the path to the folder you just cloned to your computer
```
cd react-training
```
- Checkout branch develop
```
git checkout develop
```
- Change the path to the folder practice-three
```
cd practice-three
```
- Install npm
```
yarn install
```
1. Run storybook
```
yarn run storybook
```
- View on port: `http://localhost:6006`
2. Run project
```
yarn start
```
- View on port: `http://127.0.0.1:5173`